package com.crm.Controller;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.crm.helper.Response;
import com.crm.services.ChargebackMerchantService;
import com.crm.services.lBPSService;

@RestController
public class ChargeBackMerchantController {

	private static Logger log = LoggerFactory.getLogger(ChargeBackMerchantController.class);

	
	@Autowired
	private ChargebackMerchantService chargebackMerchantService;
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping(value = "/getChargeBackMerList", produces = "application/json")
	public String getChargeBackMerList(@RequestBody String jsonBody) throws Exception {
		
		 Response res = new Response();
		log.info("inside the chargeback:::::");
		log.info("jsonBody::::::::" + jsonBody);
		JSONObject js = new JSONObject(jsonBody);
		log.info("js::::::" + js);
		String iMerchantId = js.getString("iMerchantId");
		String iTxnId = js.getString("iTxnId");
		String FromDate = js.getString("FromDate");
		String ToDate = js.getString("ToDate");
		
		log.info("iMerchantId:::"+iMerchantId+"::iTxnId::"+iTxnId+ "::FromDate::"+FromDate+"::ToDate::"+ToDate);
		
		String getChargeBackMerListData = chargebackMerchantService.getChargeBackMerListData(iMerchantId,iTxnId,FromDate,ToDate);
		
//		log.info("getChargeBackMerListData::::::::"+getChargeBackMerListData+":::::::::");
//		log.info("getChargeBackMerListData::::::::"+getChargeBackMerListData.replaceAll("\\[", "").replaceAll("\\]","").replaceAll("^\"|\"$", ""));
		
		if(getChargeBackMerListData.replaceAll("\\[", "").replaceAll("\\]","").replaceAll("^\"|\"$", "").equalsIgnoreCase("Not Found Data")) {
			
			JSONObject response = new JSONObject();
			response.append("Error", "Not Found Data");
			
			getChargeBackMerListData = response.toString();
		}
		
		res.setStatus(true);
//        res.setMessage(message);
        res.setResult(getChargeBackMerListData);
		
		return getChargeBackMerListData;
	
	}
}
