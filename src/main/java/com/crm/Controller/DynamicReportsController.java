package com.crm.Controller;

import java.util.ArrayList;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.crm.services.DynamicReportService;
import com.google.gson.Gson;

@RestController
public class DynamicReportsController {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	DynamicReportService dynamicReportService;
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/reports")
	public String getReportsData(@RequestBody String reports) throws Exception {
		
		JSONObject js = new JSONObject(reports);
		int reportType = js.getInt("reportType");
		int reportId = js.getInt("reportId");
		String userType = js.getString("userType");
		String userId = js.getString("userId");
		String inputParameters = js.getString("inputParameters");
		ArrayList<String> arrayList = new ArrayList<String>();
		try {
			Map<String, Object> resultData = dynamicReportService.getReportsData(reportType,reportId,userType,userId,inputParameters);
			arrayList = (ArrayList<String>) resultData.get("#result-set-1");
			if (arrayList.isEmpty()) {
				return "Data does not exist";
			}

		} catch (Exception e) {
			e.printStackTrace();
		} 
			Gson gson = new Gson();

			String jsonArray = gson.toJson(arrayList);
		return jsonArray.toString();
	}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/reseller-invoice-reports")
	public Map<String, Object> getresellerInvoiceReportsData(@RequestBody String reports) throws Exception {
		
		JSONObject js = new JSONObject(reports);
		String reportType = js.getString("reportType");
		String reportId = js.getString("reportId");
		String userType = js.getString("userType");
		String userId = js.getString("userId");
		String inputParameters = js.getString("inputParameters");
		ArrayList<String> arrayList = new ArrayList<String>();
		JSONArray array = new JSONArray();
		JSONObject js1 = new JSONObject();
		Gson gson = new Gson();
		String jsonArray = null;
		try {
			
			if (reportId.equals("5")) {
			Map<String, Object> resultData = dynamicReportService.getInvoiceReportsData(reportType,reportId,userType,userId,inputParameters);
			arrayList = (ArrayList<String>) resultData.get("#result-set-1");
				if (arrayList.isEmpty()) {
					
					js1.put("Status", "No Data found");
				}else {
				js1.put("Data", arrayList);
				jsonArray = gson.toJson(arrayList);
				}
			}else {
				JSONObject item = new JSONObject();
				item.put("Merchant_Name", "NA");
				item.put("Merchant_MID", "NA");
				item.put("Payment_Options", "NA");
				item.put("Transaction_Volume", "NA");
				item.put("Merchant_MDR", "NA");
				item.put("MDR_Volume", "NA");
				item.put("GST_On_MDR", "NA");
				item.put("Merchant_Total_Charges", "NA");
				item.put("Merchant_Net_Settlement", "NA");
				item.put("System_MDR", "NA");
				item.put("Reseller_MDR", "NA");
				item.put("Reseller_Charges", "NA");
				item.put("Reseller_Applicable_Taxes", "NA");
				item.put("Reseller_Total_Charges", "NA");
				
				array.put(item);
				js1.put("Data", array);
//				jsonArray = gson.toJson(js1.toMap());
				
			}
			
			

		} catch (Exception e) {
			e.printStackTrace();
		} 
			
		return js1.toMap();
	}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/getTransaction-LifeCycleDetails")
	public Map<String, Object> getLifeCycleDetails(@RequestBody String jsonBody){
		JSONObject js = new JSONObject(jsonBody);
		JSONArray array = new JSONArray();
		JSONObject js1 = new JSONObject();
		Gson gson = new Gson();
		String jsonArray = null;
		String MerchantId= js.getString("merchantId");
		String FromDate= js.getString("fromDate");	
		String ToDate= js.getString("toDate");	
		//int SpId1= js.get("spId");	
		String SpId = String.valueOf(js.get("spId"));
		String LifeCyclecode= js.getString("lifeCycleCode");	
		String TransactionId= js.getString("transactionId");	
		String RrnNumber= js.getString("rrnNumber");	
		
		ArrayList<String> arrayList = new ArrayList<String>();
		Map<String, Object> resultData = dynamicReportService.getLifeCycleList(MerchantId, FromDate, ToDate, SpId, LifeCyclecode, TransactionId, RrnNumber);
		arrayList = (ArrayList<String>) resultData.get("#result-set-1");
			if (arrayList.isEmpty()) {
				
				js1.put("Status", "Data does not exist");
			}else {
			js1.put("Data", arrayList);
			jsonArray = gson.toJson(arrayList);
			}
		return js1.toMap();
	}
}
