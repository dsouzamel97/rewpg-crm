package com.crm.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.crm.dto.BankAccountKycDTO;
import com.crm.dto.CINKycDTO;
import com.crm.dto.GSTINKycDTO;
import com.crm.dto.KycDTO;
import com.crm.dto.PANKycDTO;
import com.crm.services.IPInfoService;
import com.crm.services.KYCServices;

import javax.websocket.server.PathParam;
import java.util.HashMap;
import java.util.Map;

@RestController
public class KYCController {
     private static final Logger logger = LoggerFactory.getLogger(KYCController.class);

 	@Autowired
 	KYCServices kycServices;
 	
/*	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
    @PostMapping(path="/GSTIN", produces = MediaType.APPLICATION_JSON_VALUE )
        public ResponseEntity<String> gstinVerify(@RequestBody GSTINKycDTO dto) {
        try {
        	ResponseEntity<String> resp= kycServices.getGSTINVerifyDetails(dto);
            return resp;
        } catch(HttpClientErrorException ex) {
            logger.info("HttpClientErrorException :::"+ex);
            return ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders()).body(ex.getResponseBodyAsString());
        }catch (HttpStatusCodeException ex) {
       	 logger.info( "HttpStatusCodeException  :" + ex.getResponseBodyAsString());
            return ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders()).body(ex.getResponseBodyAsString());
       }
    } */
	
	
    @PostMapping(path="/kyc/validate", produces = MediaType.APPLICATION_JSON_VALUE )
        public ResponseEntity<String> gstinVerify(@RequestParam("source") String source,  @RequestBody KycDTO dto) {
        try {
        	logger.info("source::::::"+source);
        	ResponseEntity<String> resp= kycServices.getKycVerifyDetails(dto, source);
            return resp;
        } catch(HttpClientErrorException ex) {
            logger.info("HttpClientErrorException :::"+ex);
            return ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders()).body(ex.getResponseBodyAsString());
        }catch (HttpStatusCodeException ex) {
       	 logger.info( "HttpStatusCodeException  :" + ex.getResponseBodyAsString());
            return ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders()).body(ex.getResponseBodyAsString());
       }
    }


/*	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
    @PostMapping(path="/CIN", produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<String> cinVerify(@RequestBody CINKycDTO dto) {


        try {
        	ResponseEntity<String> resp= kycServices.getCINVerifyDetails(dto);
            return resp;
        } 
        catch(HttpClientErrorException ex) {
            logger.info("HttpClientErrorException :::"+ex);
            return ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders()).body(ex.getResponseBodyAsString());
        }catch (HttpStatusCodeException ex) {
       	 logger.info( "HttpStatusCodeException  :" + ex.getResponseBodyAsString());
            return ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders()).body(ex.getResponseBodyAsString());
       }
        
       
    }*/

	
    @PostMapping(path="/bankAccount/validate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> bankAccountVerify(@RequestParam("source") String source,@RequestBody BankAccountKycDTO dto) {
    	try {
	ResponseEntity<String> resp= kycServices.getBankAccountVerifyDetails(dto, source);
    	return resp;
    } catch(HttpClientErrorException ex) {
        logger.info("HttpClientErrorException :::"+ex);
        return ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders()).body(ex.getResponseBodyAsString());
    }catch (HttpStatusCodeException ex) {
   	 logger.info( "HttpStatusCodeException  :" + ex.getResponseBodyAsString());
        return ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders()).body(ex.getResponseBodyAsString());
   }

    }

	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
    @PostMapping(path="/IFSC/validate" , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> ifscVerify(@RequestParam("source") String source,@RequestBody BankAccountKycDTO dto) {

        try {
          
        	ResponseEntity<String> resp= kycServices.getIFSCVerifyDetails(dto, source);
            return resp;
        } catch(HttpClientErrorException ex) {
            logger.info("HttpClientErrorException :::"+ex);
            return ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders()).body(ex.getResponseBodyAsString());
        }catch (HttpStatusCodeException ex) {
       	 logger.info( "HttpStatusCodeException  :" + ex.getResponseBodyAsString());
            return ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders()).body(ex.getResponseBodyAsString());
       }

    }
	
/*	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
    @PostMapping(path="/PAN", produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<String> PANVerify(@RequestBody PANKycDTO dto) {


        try {
        	ResponseEntity<String> resp= kycServices.getPANVerifyDetails(dto);
            return resp;
        } 
        catch(HttpClientErrorException ex) {
            logger.info("HttpClientErrorException :::"+ex);
            return ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders()).body(ex.getResponseBodyAsString());
        }catch (HttpStatusCodeException ex) {
       	 logger.info( "HttpStatusCodeException  :" + ex.getResponseBodyAsString());
            return ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders()).body(ex.getResponseBodyAsString());
       }
        
       
    } */
}

