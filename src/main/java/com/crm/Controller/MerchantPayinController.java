package com.crm.Controller;

import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.crm.services.MerchantPayinService;

@RestController
public class MerchantPayinController {
	
	private static Logger log = LoggerFactory.getLogger(MerchantPayinController.class);

	@Autowired
	MerchantPayinService merchantpayinService;
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping(value = "/insertpayoutmdr", produces = "application/json")
	public ResponseEntity<?> CreateRole1(@RequestBody String jsonBody) {
		String ResponseValue=null;
		log.info("entered insertpayoutmdr+++++++============");
		JSONObject js = new JSONObject(jsonBody);
		String merchant_id=js.getString("merchant_id");
		String sp_id=js.getString("sp_id");
		String mop=js.getString("mop");
		String aggr_mdr_Type=js.getString("aggr_mdr_Type");
		String aggr_mdr=js.getString("aggr_mdr");
		String base_mdr_Type=js.getString("base_mdr_Type");
		String base_mdr=js.getString("base_mdr");
		String start_date=js.getString("start_date");
		String end_date=js.getString("end_date");
		String mid=js.getString("mid");
		String tid=js.getString("tid");
		String min_amt=js.getString("min_amt");
		String max_amt=js.getString("max_amt");
		log.info("entered 22 insertpayoutmdr+++++++============");

		Map<String, Object> insertrecord=null;
		try {
			log.info("entered insertpayoutmdr try+++++++============");

			insertrecord=merchantpayinService.insertMerchant_payoutmdrRecord(merchant_id,sp_id,mop,aggr_mdr_Type,aggr_mdr,base_mdr_Type,base_mdr,start_date,end_date,mid,tid,min_amt,max_amt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok().body(insertrecord);
	}


	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping(value = "/DeletebyId", produces = "application/json")	
	public String DeleteMerchantpayoutbyid(@RequestBody String jsonBody) {
		String ResponseValue=null;
		JSONObject js = new JSONObject(jsonBody);
		String Id=js.getString("Id");
		String deletedata=null;
		log.info("id::"+Id);
		try {
			log.info("id  try::"+Id);

			deletedata=merchantpayinService.DeleteByID(Id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (deletedata.equals("success")) {
			JSONObject respons = new JSONObject();
			respons.put("Status", "success");
			respons.put("Reason", "Deleted Successfully.");
			ResponseValue = respons.toString();
		} else {
			JSONObject respons = new JSONObject();
			respons.put("Status", "fail");
			respons.put("Reason", "Oops, something went wrong!");
			ResponseValue = respons.toString();

			
			
			
		}
		return ResponseValue;
		}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping(value = "/Getpayoutmdr", produces = "application/json")	
	public ResponseEntity<?> getMerchantPayout(@RequestBody String jsonBody) {
		JSONObject js = new JSONObject(jsonBody);
		String admin=js.getString("admin");
		Map<String, Object> Reportdata = null;
		try {
		Reportdata=merchantpayinService.getPayoutmdr();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return ResponseEntity.ok().body(Reportdata);
	}
	
}
