package com.crm.Controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.crm.Repository.BulkRefundSqlRepo;
import com.crm.dto.BeneFiledsDto;
import com.crm.dto.PayouCallbackResponse;
import com.crm.model.MerchantList;
import com.crm.util.GeneralUtil;
import com.crm.util.S2SCall;
import com.google.gson.Gson;

@Controller
@RequestMapping("/payout")
public class MerchantPayoutCallback {

	@Autowired
	BulkRefundSqlRepo bulkRefundSqlRepo;

	@PostMapping("/PayoutCallabckHandler")
	public String PayotBulkRaised(@RequestBody String PayotBulkRaised) {
		{
			Gson gson = new Gson();
			String merchant_id = null;
			JSONArray jsonArray = new JSONArray();
			// JSONObject Reqreasy = new JSONObject();
			Map<String, String> Reqreasy = new LinkedHashMap<>();

			JSONObject pusHResponse = new JSONObject();

			PayouCallbackResponse CallbackResponse = gson.fromJson(PayotBulkRaised, PayouCallbackResponse.class);

			String TransStatus = CallbackResponse.getData().get(0).getTransactionStatus();
			String RefNo = CallbackResponse.getData().get(0).getTransactionStatus();
			String Utr = CallbackResponse.getData().get(0).getUtr();
			// Map<String, Object> GetDetails=
			// bulkRefundSqlRepo.getPayoutTransDeatils(RefNo);

			if (TransStatus.equalsIgnoreCase("Porcessing")) {
				int i = bulkRefundSqlRepo.UpdatePayoutTransResponse(TransStatus, Utr, "Y", RefNo);

			} else if (TransStatus.equalsIgnoreCase("SUCCESS")) {
				int i = bulkRefundSqlRepo.UpdatePayoutTransResponse(TransStatus, Utr, "Y", RefNo);
			} else {

			}
			// Map<String, Object> GetResDeatisl=
			// bulkRefundSqlRepo.getPayoutTransDeatils(RefNo);
			pusHResponse.put("payout_status", TransStatus);
			pusHResponse.put("CustRefNo", RefNo);
			pusHResponse.put("UTR_No", Utr);
			pusHResponse.put("statusDescription", CallbackResponse.getData().get(0).getStatusDescription());
			pusHResponse.put("Account_No", "");
			S2SCall.ServerToServerCallMerchant("", pusHResponse.toString());

			return "SUCCESS";
		}
	}

}
