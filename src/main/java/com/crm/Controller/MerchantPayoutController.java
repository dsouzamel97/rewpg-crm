package com.crm.Controller;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crm.Repository.MerchantRepository;
import com.crm.dto.KycDTO;
import com.crm.dto.MerchantCreationDto;
import com.crm.model.MerchantList;
import com.crm.services.BenefieryService;
import com.crm.services.MerchantService;
import com.crm.services.ValidatorService;
import com.crm.util.GeneralUtil;

@RestController
public class MerchantPayoutController {
	private static Logger log = LoggerFactory.getLogger(PayoutGenerateController.class);

	@Autowired
	BenefieryService benefieryService;

	@Autowired
	ValidatorService validatorService;

	@Autowired
	MerchantRepository merchantRepository;

	@PostMapping("/AddBeneDeatails")
	public String AddBeneDtails(@RequestBody String BeneFileds) {
		{
			JSONObject js = new JSONObject();

			try {
				return benefieryService.AddBene(BeneFileds);
			} catch (Exception e) {
				log.info("Exception Show while Inserting data:::{}{}{} ", e);
				js.put("Error_msg", "Unable to Add details");
				js.put("Status", "False");

			}

			return js.toString();

		}
	}

	@PostMapping("/GetBaneDetails")
	public String GetBeneList(@RequestBody String BeneFileds) {
		{
			JSONObject js = new JSONObject();

			try {
				return benefieryService.GetBeneList(BeneFileds);
			} catch (Exception e) {
				log.info("Exception Show while Inserting data:::{}{}{} ", e);
				js.put("Error_msg", "Unable to Get details");
				js.put("Status", "False");

			}

			return js.toString();

		}
	}

	@PostMapping("/beneficiaryadd")
	public String ValidateAddBanko(@RequestBody String PayoutDetails) {
		{
			JSONObject js = new JSONObject(PayoutDetails);
			String Mid = js.getString("AuthID");
			String EncReq = js.getString("EncReq");

			try {
				// MerchantDetails = MerchantService.GetCreationDetails(Mid);
				MerchantList merchantList = merchantRepository.findByMerchantId(Mid);
				String Decrypt_response = GeneralUtil.decrypt2(merchantList.getTransaction_key(),
						merchantList.getTransaction_key().substring(0, 16), EncReq);
				JSONObject resp = benefieryService.addOrValidateService(Decrypt_response, Mid);
				log.info("Final Response return to merchant");
				return resp.toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "Somthing wrong";
	}

	@PostMapping("/PayoutBulkRaised")
	public String PayotBulkRaised(@RequestBody String PayotBulkRaised) {
		{
			JSONObject js = new JSONObject(PayotBulkRaised);
			JSONArray jr = new JSONArray();
			String Mid = js.getString("AuthID");
			String EncReq = js.getString("EncReq");
			JSONObject resp = new JSONObject();
			try {
				// MerchantDetails = MerchantService.GetCreationDetails(Mid);
				MerchantList merchantList = merchantRepository.findByMerchantId(Mid);
				String Decrypt_response = GeneralUtil.decrypt2(merchantList.getTransaction_key(),
						merchantList.getTransaction_key().substring(0, 16), EncReq);
				boolean ISValid = validatorService.ValidRequest(Decrypt_response);
				if (ISValid) {
					resp = benefieryService.PayoutRaisedAll(Decrypt_response, Mid);
				} else {
					resp.put("Status", "FAILED");
					resp.put("RespMessage", "Invalid transaction request.");
					resp.put("RespCode", "400");
					resp.put("ResponseData", jr);
				}
				String EncryptValue = GeneralUtil.encrypt(merchantList.getTransaction_key(),
						merchantList.getTransaction_key().substring(0, 16), resp.toString());
				JSONObject finalRespons = new JSONObject();
				finalRespons.put("AuthID", Mid);
				finalRespons.put("EncData", EncryptValue);
				log.info("Final Response return to merchant for status");
				log.info("Final Response return to merchant");
				// return finalRespons.toString();
				return finalRespons.toString();
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		return "Somthing Went wrong";
	}

	@PostMapping("/checkbalance")
	public String WalletBalance(@RequestBody String WalletBalance) {
		{
			JSONObject js = new JSONObject(WalletBalance);
			String Mid = js.getString("AuthID");
			String EncReq = js.getString("EncReq");

			try {
				// MerchantDetails = MerchantService.GetCreationDetails(Mid);
				MerchantList merchantList = merchantRepository.findByMerchantId(Mid);

				String Decrypt_response = GeneralUtil.decrypt2(merchantList.getTransaction_key(),
						merchantList.getTransaction_key().substring(0, 16), EncReq);
				JSONObject Req1 = new JSONObject(Decrypt_response);

				String resp = benefieryService.CheckWalletBalance(Req1.getString("AuthID"), Req1.getString("Type"));

				String EncryptValue = GeneralUtil.encrypt(merchantList.getTransaction_key(),
						merchantList.getTransaction_key().substring(0, 16), resp);
				JSONObject finalRespons = new JSONObject();
				finalRespons.put("AuthID", Mid);
				finalRespons.put("EncData", EncryptValue);
				log.info("Final Response return to merchant for status");
				log.info("Final Response return to merchant");
				return finalRespons.toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "Somthing went wrong";
	}

	@PostMapping("/payoutstatus")
	public String CheckPayoutStatus(@RequestBody String SatusList) {
		{
			JSONObject js = new JSONObject(SatusList);
			String Mid = js.getString("AuthID");
			String EncReq = js.getString("EncReq");
			try {
				// MerchantDetails = MerchantService.GetCreationDetails(Mid);
				MerchantList merchantList = merchantRepository.findByMerchantId(Mid);
				String Decrypt_response = GeneralUtil.decrypt2(merchantList.getTransaction_key(),
						merchantList.getTransaction_key().substring(0, 16), EncReq);
				JSONObject Req = new JSONObject(Decrypt_response);

				String resp = benefieryService.PayoutStatusApi(Req.getString("AuthID"), Decrypt_response);

				String EncryptValue = GeneralUtil.encrypt(merchantList.getTransaction_key(),
						merchantList.getTransaction_key().substring(0, 16), resp);
				JSONObject finalRespons = new JSONObject();
				finalRespons.put("AuthID", Mid);
				finalRespons.put("EncData", EncryptValue);
				log.info("Final Response return to merchant for status");
				return finalRespons.toString();
			} catch (Exception e) {
				log.info("Exception while get status Api::{}{}", e);
			}
		}
		return "Somthing went wrong";
	}
}
