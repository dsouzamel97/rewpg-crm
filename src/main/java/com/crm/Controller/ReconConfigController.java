package com.crm.Controller;

import java.sql.Timestamp;
import java.util.List;

import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.crm.Repository.ReconManagementRepo;
import com.crm.model.TblMstreconconfig;
import com.crm.services.ReconManagementService;

@RestController
public class ReconConfigController {

	private static org.slf4j.Logger log = LoggerFactory.getLogger(ReconConfigController.class);

	@Autowired
	private ReconManagementService reconManagementService;
	
	@Autowired
	ReconManagementRepo reconManagementRepo;
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping(value="/getReconConfigList" , produces = "application/json")
	public List<TblMstreconconfig> getReconConfigList(@RequestBody String requst) {
	
		List<TblMstreconconfig> reconConfigs = null;
		try {
			JSONObject js = new JSONObject(requst);
			String username = js.getString("username");

			log.info("username === " + username);
			
			 reconConfigs = reconManagementService.getReconConfigsAllData(null, null);
				log.info("reconConfigs:::::::::"+reconConfigs);

			log.info("reconConfigs end size::::::" + reconConfigs.size());

			
			return reconConfigs;
		} catch (Exception e) {
			log.info("getting exception on retrive data::::::", e);
		}
		return reconConfigs;
	}
	
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping(value="/addReconConfigData" , produces = "application/json")
	public String addReconConfigData(@RequestBody TblMstreconconfig reconConfigs) {
		
		JSONObject response = new JSONObject();
		try {
			
			String responseData = reconManagementService.addDataInReconConfig(reconConfigs);
			log.info("responseData:::::::::"+responseData);
			if(responseData.equalsIgnoreCase("success")) {
				response.put("status", "true");
				response.put("message", "Saved Recon Config data successfully");
			}
			else {
				response.put("status", "failed");
				response.put("message", "Recon Config data is not saved");
			}
			
			
			return response.toString();
		} catch (Exception e) {
			log.info("getting exception on retrive data::::::", e);
		}
		return response.toString();
	}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping(value="/editReconConfigData" , produces = "application/json")
	public String editReconConfigData(@RequestBody TblMstreconconfig reconConfigs) {
	
		JSONObject response = new JSONObject();
		try {
			
			String responseData = reconManagementService.editDataInReconConfig(reconConfigs);
			log.info("responseData:::::editReconConfigData::::"+responseData);
			if(responseData.equalsIgnoreCase("success")) {
				response.put("status", "true");
				response.put("message", "Updated Recon Config data successfully");
			}
			else {
				response.put("status", "failed");
				response.put("message", "Recon Config data is not updated");
			}
			
			
			return response.toString();
		} catch (Exception e) {
			log.info("getting exception on retrive data::::::", e);
		}
		return response.toString();
	}
	
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping(value="/getColumnNamesReconConfig" , produces = "application/json")
	public List<String> getColumnNamesReconConfig(@RequestBody String requst) {
	
		List<String> columnNames = null;
		try {
			JSONObject js = new JSONObject(requst);
			String username = js.getString("username");

			log.info("username ===> " + username);
			 columnNames = reconManagementService.getColumnNames();
				log.info("columnNames:::::::::"+columnNames);

			
			return columnNames;
		} catch (Exception e) {
			log.info("getting exception on retrive data::::::", e);
		}
		return columnNames;
	}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping(value="/deleteReconConfig" , produces = "application/json")
	public String deleteReconConfig(@RequestBody String requst) {
	
		JSONObject response = new JSONObject();
		try {
			JSONObject js = new JSONObject(requst);
			String idVal = js.getString("id");
			log.info("id ===> " + idVal);
			String username = js.getString("username");
			int idvalue = Integer.valueOf(idVal);
			log.info("username ===> " + username);
			
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			log.info("timestamp:::::::::"+timestamp);
			
			
			String Is_Deleted = "Y";
			int status = reconManagementRepo.deleteReconConfigById(timestamp,username,Is_Deleted,idvalue);
			log.info("status::::::::"+status);
			if (status == 1)
			{
				response.put("status", "true");
				response.put("message", "Recon Config data is deleted !");
				
			} 
			else
			{
				response.put("status", "false");
				response.put("message", "Recon Config data is not deleted !");
			}
			
			log.info("response::::::"+response);
			
			return response.toString();
		} catch (Exception e) {
			log.info("getting exception on retrive data::::::", e);
		}
		return response.toString();
	}
}

