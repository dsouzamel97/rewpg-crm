package com.crm.Controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.crm.model.RefundAmtRequest;
import com.crm.model.RefundTransaction;
import com.crm.services.RefundTransactionService;

@RestController
public class RefundController {
	static Logger log = LoggerFactory.getLogger(RefundController.class);
	@Autowired
	private com.crm.services.RefundTransactionService refundTransService;
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/GetRaiseRefundTranssaction-List")
	public ResponseEntity<?>  getRefundList(@RequestBody String jsonBody)
	{
		JSONObject js = new JSONObject(jsonBody);
		Map<String, Object> refundTransaction = null;
		try {
			refundTransaction = refundTransService.getRefundTransactionList(js.getString("1Payid"), js.getString("merchanTransactionId"), js.getString("merchantId") ,
					js.getString("fromDate") , js.getString("toDate") , js.getString("bankId") , js.getString("custMail") , js.getString("custMobile") );
		} catch (JSONException e) {
			
			e.printStackTrace();
			log.info("Refund List json Error   " + js + e.getMessage());
		} catch (ParseException e) {
			
			e.printStackTrace();
			log.info("Refund List Error   " + e.getMessage());
		}
		return  ResponseEntity.ok(refundTransaction);
	}
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/RaiseRefund")
	public ResponseEntity<Object> insertRefundAmt(@RequestBody String jsonBody)
	{
		JSONArray jsonarray = new JSONArray(jsonBody);
		Object raiseRefundList = null;
		try {
			raiseRefundList = refundTransService.createRefundAmtt(jsonarray);
		}catch(Exception ex) {
			ex.printStackTrace();
			log.info("RaiseRefund Error   " + ex.getMessage());
		}	
		return ResponseEntity.ok().body(raiseRefundList);
	}
	
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/BulkRefund")
	public ResponseEntity<?> BulkRefundDetails(@RequestParam("file") MultipartFile file, String userId)
	{
			
		Map<String, Object> bulkRefundFileList = null;
		try {
			bulkRefundFileList = refundTransService.bulkRefundAmtCreate(file, userId);
			
		}catch(Exception ex) {
			ex.printStackTrace();
			log.info("BulkRefund Error   " + ex.getMessage());
		}
		
		return ResponseEntity.ok().body(bulkRefundFileList);
	}
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/DownloadManual-RefundFileFromList")
	public ResponseEntity<?> DownloadManualRefundList(@RequestBody String jsonBody)
	{
		JSONArray jsonarray = new JSONArray(jsonBody);
		Map<String, Object> refundStatusRequest = null;
		try {
			refundStatusRequest = refundTransService.getRefundDownloadFileFomList(jsonarray);
			
		}catch(Exception ex) {
			ex.printStackTrace();
			log.info("BulkRefund Error   " + ex.getMessage());
		}
		
		return ResponseEntity.ok().body(refundStatusRequest);
	}
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/DownloadManual-RefundFile")
	public ResponseEntity<?> DownloadManualRefundFile(@RequestBody String jsonBody)
	{
		JSONObject js = new JSONObject(jsonBody);
		Map<String, Object> refundStatusRequest = null;
		try {
			refundStatusRequest = refundTransService.getRefundDownloadRecordsList(js.getString("1Payid"), js.getString("merchanTransactionId"), js.getString("merchantId") ,
					js.getString("fromDate") , js.getString("toDate") , js.getString("spId") , js.getString("custMail") , js.getString("custMobile"), js.getString("count"), 
					js.getInt("pageNo"), js.getInt("Type"), js.getString("SearchBy"), js.getString("refundType") );
			
		}catch(Exception ex) {
			ex.printStackTrace();
			log.info("BulkRefund Error   " + ex.getMessage());
		}
		
		return ResponseEntity.ok().body(refundStatusRequest);
	}
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/RefundRequestStatus")
	public ResponseEntity<?> RefundRequestStatus(@RequestBody String jsonBody)
	{
		JSONObject js = new JSONObject(jsonBody);
		Map<String, Object> refundStatusRequest = null;
		try {
			refundStatusRequest = refundTransService.getRefundTransactionStatusList(js.getString("1Payid"), js.getString("merchanTransactionId"), js.getString("refundId") ,
					js.getString("fromDate") , js.getString("toDate") , js.getString("refundType") , js.getString("refundStatus") ,  js.getString("count"), 
					js.getInt("pageNo"), js.getInt("Type"), js.getString("SearchBy") );
			
		}catch(Exception ex) {
			ex.printStackTrace();
			log.info("BulkRefund Error   " + ex.getMessage());
		}
		
		return ResponseEntity.ok().body(refundStatusRequest);
	}
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/Upload-ManualRefundFile")
	public ResponseEntity<?> RefundUploadManual(@RequestParam("file") MultipartFile file, String userId, String sPID)
	{
		
		Map<String, Object> uploadManualFile = null;
		try {
			uploadManualFile = refundTransService.uploadManualRefundFile(file, userId, sPID);
			
		}catch(Exception ex) {
			ex.printStackTrace();
			log.info("Upload Manual Refund File Error   " + ex.getMessage());
		}
		
		return ResponseEntity.ok().body(uploadManualFile);
	}
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/AtomRefund-FromFile")
	public ResponseEntity<?> AtomRefundFromFile(@RequestParam("file") MultipartFile file, String userId)
	{
		
		Map<String, Object> atomRefund = null;
		try {
			atomRefund = refundTransService.atomRefundFile(file, userId);
			
		}catch(Exception ex) {
			ex.printStackTrace();
			log.info("Upload Manual Refund File Error   " + ex.getMessage());
		}
		
		return ResponseEntity.ok().body(atomRefund);
	}
}
