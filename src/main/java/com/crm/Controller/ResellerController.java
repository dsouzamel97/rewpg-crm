package com.crm.Controller;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.crm.Repository.ResellerRepository;
import com.crm.dto.BusinessTypeDto;
import com.crm.dto.MerchantDocDto;
import com.crm.dto.ResellerBankAccountDto;
import com.crm.dto.ResellerBasicSetupDto;
import com.crm.dto.ResellerDto;
import com.crm.dto.ResellerKycDocDto;
import com.crm.dto.ResellerListPaginationDto;
import com.crm.dto.UploadDocDetailsDto;
import com.crm.dto.UploadDocDto;
import com.crm.model.MerchantKycDoc;
import com.crm.model.ResellerBankAcc;
import com.crm.model.ResellerEntity;
import com.crm.services.EmailService;
import com.crm.services.ResellerService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })


@RestController

public class ResellerController {
	private final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	public static String uploadDirectory = "/home/KYCDOCUMENTS";
	
	@Autowired
	private ResellerService resellerService;
	
	@Autowired
    private EmailService emailService;
	
	
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	 @PostMapping("/create-update-reseller")
	public ResellerDto createUpdateReseller(@RequestBody ResellerDto resellerReqDto) {
    	ResellerDto respResellerDto = null;
    	
    	try {
			respResellerDto = resellerService.createUpdateReseller(resellerReqDto);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
    	
    	return respResellerDto;
    	
	}
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/reseller-view-list")
	public ResellerListPaginationDto ResellerListDetails(@RequestBody String jsonBody) {
		JSONObject js = new JSONObject(jsonBody);
		String merchantName = js.getString("rname");
		String merchantId = js.getString("rid");
		String fromDate = js.getString("startDate");
		String toDate = js.getString("endDate");
		int norecord = js.getInt("pageRecords");
		int pageno =js.getInt("pageNumber");
		
		ResellerListPaginationDto resellerDetails = null;
		try {
			resellerDetails = resellerService.GetCreationDetails(fromDate, toDate, merchantId, merchantName, norecord, pageno); 
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resellerDetails;
	}
	
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/get-reseller-detail-by-resellerid")
	public ResellerDto getListbyPId(@RequestBody String jsonBody) {
		JSONObject js = new JSONObject(jsonBody);
		String resellerId = js.getString("resellerId");
		ResellerDto resellerDetails = null;
		try {

			resellerDetails = resellerService.getResellerPersonalDetailByResellerId(resellerId);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return resellerDetails;
	}

    
    @CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/create-reseller-bank")
	public ResellerBankAccountDto postResellerBankData(@RequestBody ResellerBankAcc resellerBankAcc) {
    	ResellerBankAccountDto resellerBankinfo = null;
		try {
			resellerBankinfo = resellerService.createResellerBankDetails(resellerBankAcc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resellerBankinfo;
	}

	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/update-reseller-bank")
	public ResellerBankAccountDto postResellerBankUpdt(@RequestBody ResellerBankAcc resellerBankAcc) {
		ResellerBankAccountDto resellerBankupdtinfo = null;
		try {
			resellerBankupdtinfo = resellerService.updateResellerBankDetails(resellerBankAcc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resellerBankupdtinfo;
	}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/get-reseller-bankacc-by-resellerid")
	public ResellerBankAccountDto getListBankAccbyPId(@RequestBody String jsonBody) {
		JSONObject js = new JSONObject(jsonBody);
		String resellerId = js.getString("resellerId");
		ResellerBankAccountDto resellerBankDetails = null;
		try {

			resellerBankDetails = resellerService.getResellerBankDetailByResellerId(resellerId);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return resellerBankDetails;
	}
	
	
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/reseller-view-list-bankacc")
	public List<ResellerBankAcc> ResellerListBankAccDetails() {
		
		List<ResellerBankAcc> resellerListBankAccDetails = null;
		try {
			resellerListBankAccDetails = resellerService.GetViewListOfBankAccOfReseller(); 
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resellerListBankAccDetails;
	}
	
	
	
	
	// soft delete only
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/delete-reseller-detail-byid")
	public Optional<ResellerEntity> deleteResellerPersonaDetail(@RequestBody String jsonBody) {
		JSONObject js = new JSONObject(jsonBody);
		BigInteger Pid = js.getBigInteger("id");
		Optional<ResellerEntity> resellerInfo = null;
		try {
			resellerInfo = resellerService.deleteResellerPersonaDetail(Pid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resellerInfo;
	}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/delete-resellerBank-byid")
	public List<ResellerBankAccountDto> deleteResellerBankAcc(@RequestBody String jsonBody) {
		JSONObject js = new JSONObject(jsonBody);
		BigInteger Pid = js.getBigInteger("id");
		String resellerId = js.getString("resellerId");
		List<ResellerBankAccountDto> resellerBankDetails = null;
		try {
			resellerBankDetails = resellerService.deleteResellerBankAcc(Pid, resellerId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resellerBankDetails;
	}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/resellerKyc-docUpload")
	public List<ResellerKycDocDto> kycDocumentUpload(@RequestParam("imageFile") MultipartFile imageFile , String resellerId, String docType, String docId, String username) {

		StringBuilder filesname = new StringBuilder();
		String ResellerId = resellerId;
		String DocType = docType;
		String DocId = docId;
		String User = username;
		String fileOrgname = imageFile.getOriginalFilename();
		String filesname1 = uploadDirectory + "/" + ResellerId +"/";
		fileOrgname= fileOrgname.replaceAll("\\s", "");
		String nameFileconvert = DocId +"_"+fileOrgname;
		
		File file = new File(filesname1);
		if (!file.exists()) {            
            file.mkdirs();            
        }		
		List<ResellerKycDocDto> resellerKycDocList = new ArrayList<>();
		
		
		try {
			Path path = Paths.get(filesname1,nameFileconvert);
			filesname.append(nameFileconvert);
			Files.write(path, imageFile.getBytes());
			String DocRef = filesname1 + nameFileconvert;
			ResellerKycDocDto resellerKycDoc = new ResellerKycDocDto();
			
			
			resellerKycDoc.setDocName(nameFileconvert);
			resellerKycDoc.setDocType(DocType);
			resellerKycDoc.setDocpath(DocRef);
			resellerKycDoc.setResellerId(ResellerId);
			resellerKycDoc.setModifiedBy(User);
			long millis = System.currentTimeMillis();
			String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date(millis));
			resellerKycDoc.setModifiedOn(timeStamp);
			resellerKycDoc.setUpdatedBy(User);
			resellerKycDoc.setUpdateOn(timeStamp);			

			resellerKycDocList.add(resellerKycDoc);
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		return resellerKycDocList;		
	
	}
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/reseller-insertKycdocs-details")
	public List<ResellerKycDocDto> uploadImage(@RequestParam String resellerId, String documentDetails  ) {
		

		String MerchantId = resellerId;
		String DocType = documentDetails;		
		List<ResellerKycDocDto> resellerkycdto = null;

		try {

					
			resellerkycdto = resellerService.insertKycDoc(MerchantId, DocType);
	
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		return resellerkycdto;
		
	}
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/reseller-insertKycdocs-detailsV2")
	public List<ResellerKycDocDto> uploadImageV2(@RequestBody UploadDocDto uploadDoc) {
		

		String MerchantId = uploadDoc.getMerchantId();
		Map<String,UploadDocDetailsDto> docTypes = new HashMap<>();
		
		int i = 0;
        for(UploadDocDetailsDto dto : uploadDoc.getDocType())
        {
            docTypes.put(String.valueOf(i), dto);
            i++;
        }
		List<ResellerKycDocDto> resellerkycdto = null;

		try {
			String DocType = mapper.writeValueAsString(docTypes);
					
			resellerkycdto = resellerService.insertKycDoc(MerchantId, DocType);
	
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		return resellerkycdto;
		
	}
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/getResellerKyc-DocumentList")
	public List<BusinessTypeDto> getDocumentList(@RequestBody String jsonBody) {
		
		JSONObject js = new JSONObject(jsonBody);		
		int businesstypeId = js.getInt("businesstypeId");
		String resellerId = js.getString("resellerId");
		
		List<BusinessTypeDto> bTypeDto = null;
		try {
			
			bTypeDto = resellerService.getKycDocumentList(businesstypeId, resellerId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return bTypeDto;
		
	}
	
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/download-reseller-uploadfiles")
	
	public String getuploadFiles(@RequestParam String urlfile, String docname) throws IOException {	

		String urlfile1 = urlfile;
		String fileName = docname;
		Path path = Paths.get(urlfile1);
		File file = new File(urlfile1);
		byte[] fileContent = Files.readAllBytes(file.toPath());		
		return Base64.getEncoder().encodeToString(fileContent);
		
	}
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/delete-resellerupload-image")
	public List<ResellerKycDocDto> deleteImage(@RequestBody String jsonBody) {
		JSONObject js = new JSONObject(jsonBody);
		int Id = js.getInt("id");
//		String docName = js.getString("docname");
		String path = js.getString("path");
		String ResellerId = js.getString("resellerId");
		List<ResellerKycDocDto> kycdto = null;
		ResellerKycDocDto merchantKycDoc = new ResellerKycDocDto();
		try {

//			merchantKycDoc.setDocName(docName);
			merchantKycDoc.setId(Id);
			merchantKycDoc.setDocpath(path);
			merchantKycDoc.setResellerId(ResellerId);
			kycdto = resellerService.deleteuploaddoc(merchantKycDoc);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return kycdto;

	}
	@CrossOrigin(origins = {"http://localhost:4200","https://pa-preprod.1pay.in"})
	@PostMapping("/update-Reseller-BasicSetup")
	public List<ResellerBasicSetupDto> updByMerchantId(@RequestBody String jsonBody) {
		
		JSONObject js = new JSONObject(jsonBody);		
		String ressellerId = js.getString("ressellerId");
		String returnUrl = js.getString("returnUrl");
		String isAutoRefund = js.getString("isAutoRefund");
		String hours = js.getString("hours");
		String minutes = js.getString("minutes");
		String isPushUrl = js.getString("isPushUrl");
		String pushUrl = js.getString("pushUrl");
		String settlementCycle = js.getString("settlementCycle");
		String resellerDashboardRefund = js.getString("resellerDashboardRefund");
		String rsDisableRefundCc = js.getString("rsDisableRefundCc");
		String rsDisableRefundDc = js.getString("rsDisableRefundDc");
		String rsDisableRefundNb = js.getString("rsDisableRefundNb");
		String rsDisableRefundUpi = js.getString("rsDisableRefundUpi");
		String rsDisableRefundWallet = js.getString("rsDisableRefundWallet");
		String refundApi = js.getString("refundApi");
		String refundApiDisableCc = js.getString("refundApiDisableCc");
		String refundApiDisableDc = js.getString("refundApiDisableDc");
		String refundApiDisableNb = js.getString("refundApiDisableNb");
		String refundApiDisableUpi = js.getString("refundApiDisableUpi");
		String refundApiDisableWallet = js.getString("refundApiDisableWallet");
		String integrationType = js.getString("integrationType");
		String isretryAllowed = js.getString("isretryAllowed");
		String bpsEmailNotification = js.getString("bpsEmailNotification");
		String bpsSmsNotification = js.getString("bpsSmsNotification");
		String bpsEmailReminder = js.getString("bpsEmailReminder");
		String reportCycling = js.getString("reportCycling");
		List<ResellerBasicSetupDto> resBasicSetup = null;
		try {
			
			resBasicSetup = resellerService.setResellerBasicInfo(ressellerId, returnUrl, isAutoRefund, hours, minutes, isPushUrl, pushUrl, 
					settlementCycle, resellerDashboardRefund, rsDisableRefundCc, rsDisableRefundDc, rsDisableRefundNb, rsDisableRefundUpi,
					rsDisableRefundWallet, refundApi, refundApiDisableCc, refundApiDisableDc, refundApiDisableNb, refundApiDisableUpi, 
					refundApiDisableWallet, integrationType, isretryAllowed, bpsEmailNotification, bpsSmsNotification, bpsEmailReminder,reportCycling );
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resBasicSetup;
		
	}
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/ChangeResellerStatus")
	public String ChangeResellerStatus(@RequestBody String jsonBody) {
		JSONObject js = new JSONObject(jsonBody);
		Date date = Calendar.getInstance().getTime();  
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
		String Date = dateFormat.format(date);  
		String Rid = js.getString("Rid");
		String Status = js.getString("Status");
		String Remark = js.getString("Remark");
		String Type= js.getString("Approvel_type");
		String Added_By= js.getString("Added_By");

		String ResellerDetails = null;
		try {
		if(Status.equalsIgnoreCase("Suspended") && Type.equalsIgnoreCase("Suspended"))
		{
			 Date= js.getString("Date");
		}
		

		ResellerDetails = resellerService.Reseller_Status_change(Status,Remark,Rid,Type,Date,Added_By);
		if(Rid!=null && !Rid.isBlank() && Status.equalsIgnoreCase("Active") && Type.isBlank()) {
			System.out.println("ResellerDetails:::"+ResellerDetails);
			
		
			if(ResellerDetails!=null) {
				
				JSONArray jsonArray = new JSONArray(ResellerDetails);
				
	            for(int i=0;i<jsonArray.length();i++)
	            {
	                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
	                String resellerId = jsonObject1.optString("reseller_id");
	                String resellerName = jsonObject1.optString("reseller_name");
	                String emailId = jsonObject1.optString("reseller_email_id");
	                String merReturnUrl = jsonObject1.optString("return_url");
	                String password = jsonObject1.optString("password");
	                String oldStatus = jsonObject1.optString("oldStatus");
	                String emailTrigger = jsonObject1.optString("emailTrigger");
	                String transactionKey = "";

	                
	                if(emailTrigger.equalsIgnoreCase("0") && oldStatus.equalsIgnoreCase("Pending")) {
	                 emailService.sendresellerMessage(resellerId, resellerName, emailId, merReturnUrl, password, transactionKey);
	                	}
	            	}
	           
				}
			}
		 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResellerDetails;
	}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/getFinancial-year")
	public ResponseEntity<?> getFinanacialYears(@RequestBody String jsonBody){		
		Date date = Calendar.getInstance().getTime();  
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
		String Date = dateFormat.format(date);
		Map<String, Object> responseMsg= null;
		try {
			responseMsg= resellerService.getFinanacialYear(Date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(responseMsg);
	}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/getFinancial-Month")
	public ResponseEntity<?> getFinanacialMonth(@RequestBody String jsonBody){
		JSONObject js = new JSONObject(jsonBody);
		String fYear= js.getString("fYearName");
		Map<String, Object> responseMsg= null;
		try {
			responseMsg= resellerService.getFinanacialMonth(fYear);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(responseMsg);
	}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/getMonth-date")
	public ResponseEntity<?> getFinanacialMonthDate(@RequestBody String jsonBody){
		JSONObject js = new JSONObject(jsonBody);
		String fMonth= js.getString("fmonthName");	
		Map<String, Object> responseMsg= null;
		try {	
			responseMsg= resellerService.getFinanacialMonthDt(fMonth);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(responseMsg);
	}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/AttachmentUpload")
	public ResponseEntity<?> getAttachmentUpload(@RequestParam("imageFile") MultipartFile imageFile , String MerchantId, String AddedBy){
		Map<String, Object> responseMsg= null;
		try {			
			responseMsg= resellerService.getAttachmentUploafFile(imageFile, MerchantId, AddedBy);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(responseMsg);
	}
	
	@CrossOrigin(origins = { "http://localhost:4200", "https://pa-preprod.1pay.in" })
	@PostMapping("/AttachmentUpload-List")
	public ResponseEntity<?> getAttachmentUploadList(@RequestBody String jsonBody){
		JSONObject js = new JSONObject(jsonBody);
		String merchantId= js.getString("MerchantId");
		Map<String, Object> responseMsg= null;
		try {
			responseMsg= resellerService.getAttachmentUploafList(merchantId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(responseMsg);
	}
}
