package com.crm.config;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.ExpiredJwtException;

@Component
public class JwtAuthFilter extends OncePerRequestFilter {

	@Autowired
	private com.crm.helper.JwtHelperUtil JwtHelperUtil;
	
	@Autowired
	private com.crm.services.customUserDetailsService customUserDetailsService;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
	String RequestTokenHeader= request.getHeader("Authorization");
	 System.out.println("JwtAuthFilter.java::::::::::::::;Token is Validated"+RequestTokenHeader);

	 String Username=null;
	 String JwtToken=null;
	 if(RequestTokenHeader !=null &&RequestTokenHeader.startsWith("Bearer "))
	 {
		 JwtToken = RequestTokenHeader.substring(7);
		 try {

			 if(!JwtHelperUtil.isTokenExpired(JwtToken))
			 {
				 
				Date dat=  JwtHelperUtil.extractExpiration(JwtToken);
				
				System.out.print("dat Expirantaion::::::::::::::::::::: "+dat);
				/*
				 * ExpiredJwtException ex = null; String isRefreshToken =
				 * request.getHeader("isRefreshToken"); String requestURL =
				 * request.getRequestURL().toString();
				 * 
				 * // allow for Refresh Token creation if following conditions are true. if
				 * (isRefreshToken != null && isRefreshToken.equals("true") &&
				 * requestURL.contains("refreshtoken")) {
				 * 
				 * allowForRefreshToken(ex, request); } else { request.setAttribute("exception",
				 * ex); }
				 */
			 Username= this.JwtHelperUtil.extractUsername(JwtToken);
			 System.out.println("JwtAuthFilter.java:::::::::::  "+Username);
			 if(Username !=null && SecurityContextHolder.getContext().getAuthentication()==null)
			 {		UserDetails userDetails= this.customUserDetailsService.loadUserByUsername(Username);

			 if(this.JwtHelperUtil.validateToken(JwtToken, userDetails))
			 {
				 UsernamePasswordAuthenticationToken UsernamePasswordAuthenticationToken= new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
				 UsernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				  SecurityContextHolder.getContext().setAuthentication(UsernamePasswordAuthenticationToken);
			 
			 }else {
				 System.out.print("Token is not valid");

			 }
			 }else {
				 System.out.print("Token is not Validated");
			 }
			 }
		 }catch(ExpiredJwtException ex)
		 {
			 ex.printStackTrace();
			
		 //Validate	 
	 }
	 }
	 filterChain.doFilter(request, response);
	}
	
	/**13-04-2023 as refresh token api implemented for updating user token
	 private void allowForRefreshToken(ExpiredJwtException ex, HttpServletRequest request1) {

			// create a UsernamePasswordAuthenticationToken with null values.
			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
					null, null, null);
			// After setting the Authentication in the context, we specify
			// that the current user is authenticated. So it passes the
			// Spring Security Configurations successfully.
			SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			// Set the claims so that in controller we will be using it to create
			// new JWT
			request1.setAttribute("claims", ex.getClaims());

		}
	
	**/

	
	
}
