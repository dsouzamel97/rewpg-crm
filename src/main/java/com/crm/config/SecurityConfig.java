package com.crm.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.crm.services.customUserDetailsService;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private final Logger log = LoggerFactory.getLogger(SecurityConfig.class);
	
	@Autowired
	private customUserDetailsService customUserDetailsService;

	@Autowired
	private JwtAuthFilter jwtFilter;

	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().and().cors().disable()
                .antMatcher("/crm/**")
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/forgetPassword").permitAll()
                .antMatchers(HttpMethod.POST, "/reset-password").permitAll()
                .antMatchers(HttpMethod.POST, "/get-Signup").permitAll()
                .antMatchers(HttpMethod.POST, "/verified-OTP").permitAll()
                .anyRequest().authenticated()
				 .and().httpBasic()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        
    }


	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// TODO Auto-generated method stub
		auth.userDetailsService(customUserDetailsService);
		super.configure(auth);
	}

	/*
	 * @Configuration
	 * 
	 * @EnableWebMvc public class WebConfig implements WebMvcConfigurer {
	 * 
	 * @Override public void addCorsMappings(CorsRegistry registry) {
	 * registry.addMapping("/**") // .allowedOrigins("http://localhost:4200") //
	 * .allowedMethods("*") // .allowedHeaders("*") // .allowCredentials(true)
	 * .maxAge(4800); } }
	 */ 
	
	/*
	 * @Bean public CorsConfigurationSource corsConfigurationSource() {
	 * CorsConfiguration configuration = new CorsConfiguration();
	 * configuration.setAllowedOrigins(Arrays.asList( "http://localhost:4200",
	 * "https://localhost:4200", "*" ));
	 * configuration.setAllowedMethods(Arrays.asList("GET","POST",
	 * "OPTIONS","PUT"));
	 * configuration.setExposedHeaders(Arrays.asList("Authorization"));
	 * configuration.setAllowedHeaders(Arrays.asList("Origin", "Content-Type",
	 * "Accept", "X-Requested-With", "Authorization", "remember-me"));
	 * UrlBasedCorsConfigurationSource source = new
	 * UrlBasedCorsConfigurationSource(); source.registerCorsConfiguration("/**",
	 * configuration); return source; }
	 */
/**
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				log.info("Security Configuration CommonConfigurationi for CORS");
				registry.addMapping("/**").allowedOrigins("http://localhost:4200","https://localhost:4200", "*");
			}
		};
	}
	**/
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	@Bean
	public AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();

	}

	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

}
