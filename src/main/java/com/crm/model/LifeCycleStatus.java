package com.crm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tbl_transactionlifecycle")
public class LifeCycleStatus {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private Long id;
	
	@Column(name= "TransId")
	private Long TransId;
	
	@Column(name= "transLifeCycle")
	private String transLifeCycle;
	
	@Column(name= "FileName")
	private String FileName;
	
	@Column(name= "Remarks")
	private String Remarks;
	
	@Column(name= "AddedBy")
	private String AddedBy;
	
	

}
