package com.crm.processor;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.crm.Repository.ReconManagementRepo;
import com.crm.model.TblTransactionmaster;

@Service
public class SBIAutoRefundProcessor {

	@Autowired
	ReconManagementRepo reconManagementRepo;

	@Value("${sbi.verificationUrlSBINB}")
	private String verificationUrlSBINB;

	@Value("${sbi.SBIKeyFolder}")
	private String SBIKeyFolder;

	@Value("${sbi.SBIKeyFileName}")
	private String SBIKeyFileName;

	static Logger log = LoggerFactory.getLogger(SBIAutoRefundProcessor.class);

	public String verficationCall(TblTransactionmaster txnMaster) {
		log.info("inside the verficationCall method:::::::::");
		String bankId = txnMaster.getBankId();
		String instrumentId = txnMaster.getInstrumentId();
		Double txnAmount = txnMaster.getTxnAmount();
		Double totalAmount = txnAmount + txnMaster.getSurCharge();

		
		log.info("totalAmount === " + totalAmount + "::bankId::::::"+bankId+"::::instrumentId::::"+instrumentId+"::::::txnAmount:::::"+txnAmount);
		DecimalFormat df = new DecimalFormat("0.00");
		String fTotalAmount = df.format(totalAmount);
		log.info("fTotalAmount === " + fTotalAmount);
		String amount = fTotalAmount;

		String merchantId = txnMaster.getMerchantId();
		long id = txnMaster.getId();
		Timestamp txnDate = txnMaster.getDateTime();
		String processId = txnMaster.getProcessId();
		String updateResponse = null, responseCodeVR = null;

		log.info("merchantId::::::::"+merchantId+"::::id::::"+id+"::::txnDate:::"+txnDate+":::processId::"+processId);
		String mid = null, tid = null;

		List<Object[]> spMidKeyVal = reconManagementRepo.getSPMidKey(merchantId, bankId, instrumentId, processId);

		for (Object[] obj : spMidKeyVal) {

			mid = (String) obj[0];
			log.info("mid::::::" + mid);
			tid = (String) obj[1];
			log.info("tid::::::" + tid);

		}
		log.info("mid:::1:::" + mid);
		log.info("tid:::1:::" + tid);

		log.info("merchantID = " + merchantId + "bankID = " + bankId + "instrumentID = " + instrumentId + "processID ="
				+ processId + "" + "amount " + amount + "txnDate" + txnDate);

		String initateRefund = "";
		String merchant_code = mid;
		String requestURL = "onepay_ref_no=" + id + "|amount=" + amount;// |merchant_code="+ merchant_code;

		log.info("requestURL" + requestURL);
		String checkSum = this.getCheckSum(requestURL);
		log.info("checkSum---------------" + checkSum);
		String verificationCallURL = null;

		try {
			log.info("verificationUrlSBINB:::::::::" + verificationUrlSBINB);
			verificationCallURL = verificationUrlSBINB;
			log.info("verificationCallURL:::::::::" + verificationCallURL);
		} catch (Exception e1) {
			e1.printStackTrace();
			log.info("Not get any property file variable");
		}

		String plainRequestURL = requestURL + "|checkSum=" + checkSum;
		log.info("com.payone.serviceprovider.SBIAutoRefundProcessor.java ::: plainRequestURL" + plainRequestURL);

		byte[] sbipRivateKeyInBytes = this.getSBIPRivateKeyInBytes();
		String encryptedRequest = this.encryptData(plainRequestURL, sbipRivateKeyInBytes);
		log.info("com.payone.serviceprovider.SBIAutoRefundProcessor.java ::: encryptedRequest" + encryptedRequest);

		encryptedRequest = "encdata=" + encryptedRequest + "&merchant_code=" + merchant_code;

		String verificationResponse = null;
		if (verificationCallURL != null) {
			log.info("encryptedRequest -------------" + encryptedRequest + "verificationCallURL = "
					+ verificationCallURL);
			verificationResponse = this.secureServerCallYBL(verificationCallURL, encryptedRequest);
			log.info("verificationResponse::::::::::" + verificationResponse);
		}

		if (verificationResponse != null) {
			log.info("verificationResponse -------------" + verificationResponse);
			String verificationDecryptedResponse = this.decrypt(verificationResponse, sbipRivateKeyInBytes);
			log.info("verificationDecryptedResponse -------------" + verificationDecryptedResponse);
			log.info("com.payone.serviceprovider.SBIAutoRefundProcessor.java ::: verificationDecryptedResponse"
					+ verificationDecryptedResponse);
			String[] responsedatav = verificationDecryptedResponse.split("\\|");

			log.info("array length" + responsedatav.length);
			Map<String, String> responseDataVR = new HashMap<String, String>();

			for (String dataParameter : responsedatav) {
				log.info("dataParameter" + dataParameter);
				String[] parameterAndValuePair = dataParameter.split("=");

				if (parameterAndValuePair.length == 2) {
					log.info("com.payone.serviceprovider.SBIAutoRefundProcessor.java ::: -- - "
							+ parameterAndValuePair[0] + "value = " + parameterAndValuePair[1]);
					responseDataVR.put(parameterAndValuePair[0], parameterAndValuePair[1]);
				}
			}

			responseCodeVR = responseDataVR.get("status_code");
			log.info(" ::: ===================== Success =======================================" + responseCodeVR);

			if (responseCodeVR.equals("Success")) {
				String referenceNo = responseDataVR.get("sbi_ref_no");
				String txnId = String.valueOf(id);
				int responseCodeVR1 = reconManagementRepo.updateResponse(referenceNo, referenceNo, referenceNo, txnId);
				log.info("responseCodeVR1:::::::::::" + responseCodeVR1);

				if (responseCodeVR1 == 1) {
					responseCodeVR = "Success";
				} else {
					responseCodeVR = "failure";
				}
				log.info("responseCodeVR:::::::::::" + responseCodeVR);

				DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				try {
					Date parse = new Date();
					parse.setTime(txnDate.getTime());
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.YYYY");
					String format = dateFormat.format(parse);

					log.info("format :::::::" + format);
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}

		}

		return responseCodeVR;

	}

	private String getCheckSum(String paramsList) {
		MessageDigest md;
		StringBuffer hexString = new StringBuffer();
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(paramsList.getBytes());
			byte byteData[] = md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}

			for (int i = 0; i < byteData.length; i++) {
				String hex = Integer.toHexString(0xff & byteData[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}
		} catch (NoSuchAlgorithmException e) {
			log.error(e.getMessage(), e);
		}
		return hexString.toString();
	}

	// SBIKeyReader
	public byte[] getSBIPRivateKeyInBytes() {
		byte[] keyBytes = null;
		try {

			log.info("SBIKeyFolder:::::::::::::" + SBIKeyFolder);
			log.info("SBIKeyFileName:::::::::" + SBIKeyFileName);
			String filePath = SBIKeyFolder;
			String fileName = SBIKeyFileName;
			log.info("filePath:::::::::::::" + filePath);
			log.info("fileName:::::::::" + fileName);

			File f = new File(filePath, fileName);
			log.info("f:::::::" + f);
			FileInputStream fis = new FileInputStream(f);
			DataInputStream dis = new DataInputStream(fis);
			keyBytes = new byte[(int) f.length()];
			dis.read(keyBytes);
		} catch (Exception ex) {
			log.info("-------------------------" + ex.getMessage());
		}
		return keyBytes;
	}

	private String encryptData(String data, byte[] keyBytes) {
		String encData = null;

		Cipher cipher;
		try {
			cipher = Cipher.getInstance("AES/GCM/NoPadding");
			int blockSize = cipher.getBlockSize();
			log.info("" + blockSize);
			byte[] iv = new byte[cipher.getBlockSize()];
			byte[] dataBytes = data.getBytes();
			int plaintextLength = dataBytes.length;
			int remainder = plaintextLength % blockSize;
			if (remainder != 0) {
				plaintextLength += (blockSize - remainder);
			}
			byte[] plaintext = new byte[plaintextLength];
			System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);
			SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
			SecureRandom randomSecureRandom = SecureRandom.getInstance("SHA1PRNG");
			randomSecureRandom.nextBytes(iv);
			GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);
			cipher.init(Cipher.ENCRYPT_MODE, keySpec, parameterSpec);
			byte[] results = cipher.doFinal(plaintext);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			outputStream.write(iv);
			outputStream.write(results);
			byte[] encrypteddata = outputStream.toByteArray();
			encData = java.util.Base64.getEncoder().encodeToString(encrypteddata);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage(), e);
		}
		return encData;
	}

	public static String secureServerCallYBL(String sURL, String data) {

		String line = null;
		BufferedReader br = null;
		StringBuffer respString = null;

		log.info("S2SCall.java ::: secureServerCallYBL() :: Posting URL : " + sURL + " data:::>>> " + data);
		data = data.replaceAll("\\+", "%2B");
		data = data.replaceAll("\\/", "%2F");
		log.info("S2SCall.java ::: secureServerCallYBL() remove space and slash::  " + data);
		try {
			URL obj = new URL(sURL);
			log.info("S2s::: URL>>> " + obj.toString());
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			// add reuqest header

			con.setRequestMethod("POST");
			// con.addRequestProperty("Content-Type", "application/json;charset=UTF-8");
			// con.addRequestProperty("Content-Length", data.getBytes().length+"");
			// con.addRequestProperty("Content-Length", data.getBytes().length+"");
			con.setDoOutput(true);
			con.connect();

			OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
			// log.info(new SimpleDateFormat("dd-MM-YYYY HH:mm:ss").format(new Date())+" :::
			// Data length :: " + data.getBytes().length);
			wr.write(data);
			wr.flush();

			respString = new StringBuffer();
			log.info("S2SCall.java ::: secureServerCall() :: HTTP  " + con.getResponseCode() + " HTTP msg:: "
					+ con.getResponseMessage());

			if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
				log.info("S2SCall.java ::: secureServerCall() :: HTTP OK");

				br = new BufferedReader(new InputStreamReader(con.getInputStream()));

				while ((line = br.readLine()) != null) {
					log.info("S2SCall.java ::: secureServerCall() :: Response : " + line);
					respString.append(line);
				}

				br.close();

				return respString.toString();
			}
		} catch (Exception e) {
			log.error("S2SCall.java ::: secureServerCall() :: Error Occurred while Processing Request : ", e);

		}

		return null;

	}

	public static String decrypt(String encData, byte[] key) {
		String decdata = null;
		try {
			Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
			SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
			byte[] results = Base64.getDecoder().decode(encData);
			byte[] iv = Arrays.copyOfRange(results, 0, cipher.getBlockSize());
			cipher.init(Cipher.DECRYPT_MODE, keySpec, new GCMParameterSpec(128, iv));
			byte[] results1 = Arrays.copyOfRange(results, cipher.getBlockSize(), results.length);
			byte[] ciphertext = cipher.doFinal(results1);
			decdata = new String(ciphertext).trim();

		} catch (Exception ex) {
		}
		return decdata;
	}

	public String initiateRefundMethod(String transactionId) {
		String data = "";
		try {
			
			List<Object[]> initiateRefundVal = reconManagementRepo.initiateRefund(transactionId);

			String refamount = null;
			String refundType = null;
			String txn_Id = null;
			String service_txn_id = null;
			String refundId = null;
			for (Object[] obj : initiateRefundVal) {

				txn_Id = (String) obj[0];
				log.info("txn_Id::::::::" + txn_Id);
				refamount = (String) obj[1];
				log.info("refamount::::::" + refamount);
				refundId = (String) obj[2];
				log.info("refundId::::::" + refundId);
				refundType = (String) obj[3];
				log.info("refundType::::::" + refundType);
				service_txn_id = (String) obj[4];
				log.info("service_txn_id::::::" + service_txn_id);

			}
			 data = data +service_txn_id + "|" + txn_Id +"|"+ refundId + "|" + refamount +  "\n";
			 log.info("data::::::::"+data);
		}
		catch(Exception e) {
			
		}
		
		return data;
	}
	
	
	
	
	public String getDataForReconRefundMethod(String transactionId) {
		String data = "";
		try {
			
			List<Object[]> getDataForReconRefund = reconManagementRepo.getDataForReconRefund(transactionId);

			String txn_code = null;
			String txn_Date = null;
			String refund_Date = "";
			String order_Id = null;
			String txnAmount = null;
			
			 
			
			for (Object[] obj : getDataForReconRefund) {

				txn_code = (String) obj[0];
				log.info("txn_code::::::::" + txn_code);
				txn_Date = (String) obj[1];
				log.info("txn_Date::::::" + txn_Date);
				order_Id = (String) obj[2];
				log.info("order_Id::::::" + order_Id);
				txnAmount = (String) obj[3];
				log.info("txnAmount::::::" + txnAmount);
				

			}
			data = txn_code + "|" + txn_Date + "|" + refund_Date +"|"+ order_Id + "|" + txnAmount + "|" +"\n";
			 log.info("data::::::::"+data);
		}
		catch(Exception e) {
			
		}
		
		return data;
	}

}
