package com.crm.processor;

import java.security.Key;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crm.Repository.BulkRefundSqlRepo;
import com.crm.ServiceDaoImpl.BeneServiceImpl;
import com.crm.dto.BeneFiledsDto;
import com.crm.dto.BeneFiledsDto.BeneList;
import com.crm.dto.InduspayAddBeneResponse;
import com.crm.dto.IndussPayPayoutRaised;
import com.crm.dto.InduspayAddBeneResponse.ListAddedBene;
import com.crm.dto.IndussPayPayoutRaised.PaymentList;
import com.crm.model.TblTransactionmaster;
import com.crm.services.BenefieryService;
import com.crm.util.S2SCall;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

@Service
public class indussPayPayout {
	private static Logger log = LoggerFactory.getLogger(indussPayPayout.class);

	@Autowired
	BulkRefundSqlRepo bulkRefundSqlRepo;

	@Autowired
	BeneServiceImpl beneServiceImpl;

	public JSONObject AddBeneToIndussPay(String Req) throws Exception {
		String Key = "WXNySk1WNjl6cHZQdHJKag==";
		JSONObject Reqq = new JSONObject(Req);
		Gson gson = new Gson();
		String merchant_id = null;
		JSONArray jsonArray = new JSONArray();
		// JSONObject Reqreasy = new JSONObject();
		Map<String, String> Reqreasy = new LinkedHashMap<>();

		BeneFiledsDto BeneDetails = gson.fromJson(Reqq.toString(), BeneFiledsDto.class);
		ObjectMapper bj = new ObjectMapper();
		List<Map<String, String>> JSoArry = new ArrayList<>();
		List<Map<String, String>> JSoArry2 = new ArrayList<>();
		Map<String, String> que4 = new LinkedHashMap<>();

		for (BeneList BeneList : BeneDetails.getBeneListDto()) {
			try {
				merchant_id = BeneList.getMerchantId();
				String account_num = BeneList.getAccount_number();
				String ifsc_code = BeneList.getIfsc_code();
				String bank_name = BeneList.getBank_name();
				String account_holder = BeneList.getAccount_holder();
				String mobile_no = BeneList.getMobile_no();
				String email_id = BeneList.getEmail_Id();
				String Id = BeneList.getId();
				// JSONObject que3 = new JSONObject();
//ObjectNode que3 = bj.createObjectNode();
				Map<String, String> que3 = new LinkedHashMap<>();

				que3.put("name", account_holder);
				que3.put("email", email_id);
				que3.put("mobileNumber", mobile_no);
				que3.put("beneName", account_holder);
				que3.put("beneAccountNumber", account_num);
				que3.put("beneIfsc", ifsc_code);
				que3.put("bankName", bank_name);

				/*
				 * que3.put("mobileNumber", mobile_no); que3.put("bankName", account_holder);
				 * que3.put("email", email_id); que3.put("name", account_holder);
				 * que3.put("beneAccountNumber", account_num); que3.put("bankName", bank_name);
				 * que3.put("beneIfsc", ifsc_code);
				 */
				JSoArry.add(que3);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		ObjectMapper ob = new ObjectMapper();

		String Obj1 = ob.writeValueAsString(JSoArry);
		Reqreasy.put("beneList", Obj1.replace("\\s+", ""));

		String SignKey = Createsignature(Reqreasy.toString(), Key);
		String KeyS = generateSignature(SignKey, Key);
		que4.put("signature", KeyS);
		JSoArry2.add(que4);
		Reqreasy.put("signature", KeyS);
		String Obj = ob.writeValueAsString(Reqreasy);

		String ModifieDString = Obj.replaceAll("\"\\[", "[").replaceAll("\\]\"", "]");

		log.info("Add response form Bank::::" + ModifieDString.replace("\\", ""));
		String url = "https://merchant.indusspay.com/payouts/api/v1/ind/p1/beneficiary/add";
		String Response = S2SCall.ServerToServerCallINDUSS(url, ModifieDString.replace("\\", ""));
		JSONObject Respm = new JSONObject();
		JSONArray jr = new JSONArray();
		InduspayAddBeneResponse indussAddResponse = gson.fromJson(Response, InduspayAddBeneResponse.class);
		for (ListAddedBene BeneList1 : indussAddResponse.getData()) {

			log.info("  " + merchant_id + "respo  " + BeneList1.toString());
			String ReturnResponse = beneServiceImpl.IndussPayResponseAddBene(merchant_id, BeneList1.getBeneId(),
					BeneList1.getAccountNumber(), BeneList1.getPaymentStatus(), BeneList1.getStatus());
			JSONArray jr1 = new JSONArray(ReturnResponse);
			Map<String, String> que3 = new LinkedHashMap<>();
			log.info("Add response form Bank:::::::::::::::: " + ReturnResponse);
			que3.put("Customer_Id", jr1.getJSONObject(0).getString("Customer_Id"));
			que3.put("Account_Number", jr1.getJSONObject(0).getString("accountNumber"));
			que3.put("ifsc_Code", jr1.getJSONObject(0).getString("ifscCode"));
			que3.put("bankName", jr1.getJSONObject(0).getString("bankName"));
			que3.put("payment_Status", jr1.getJSONObject(0).getString("paymentStatus"));
			que3.put("status", jr1.getJSONObject(0).getString("status"));
			que3.put("email_Id", jr1.getJSONObject(0).getString("email"));
			que3.put("Date_time", jr1.getJSONObject(0).getString("Date_time"));

			jr.put(que3);
		}
		Respm.put("beneList", jr);
		return Respm;

	}

	public JSONObject PayoutBulkRequest(String Req,String Mid) throws Exception {

		String Key = "WXNySk1WNjl6cHZQdHJKag==";
String IsApplicable = null ,ErrMsg=null,ErrCode = null;
		Gson gson = new Gson();
		IndussPayPayoutRaised payoutReq = gson.fromJson(Req, IndussPayPayoutRaised.class);

		Map<String, String> Reqreasy = new LinkedHashMap<>();
		Map<String, String> Reqreasy1 = new LinkedHashMap<>();

		Reqreasy.put("type", payoutReq.getType());
		Reqreasy.put("description", payoutReq.getDescription());
		String Checsum = payoutReq.getDescription() + payoutReq.getType();// payoutReq.checkSumValidation();
		String[] fIsrtCheck = Checsum.split("|");
		String firs = fIsrtCheck[0];
		ObjectMapper ob = new ObjectMapper();

		List<Map<String, String>> JSoArry = new ArrayList<>();

		for (PaymentList BeneList : payoutReq.getPaymentRequests()) { 
			Map<String, Object> CustomerDetails = bulkRefundSqlRepo.GetCustomerBeneId(BeneList.getCustomerId());
			Map<String, String> PaymentReq = new LinkedHashMap<>();
			PaymentReq.put("amount", BeneList.getAmount());
			PaymentReq.put("beneficiaryId", CustomerDetails.get("Bene_Id").toString());
			PaymentReq.put("txnMode", BeneList.getTxnMode());
			PaymentReq.put("clientTxnId", BeneList.getClientRefNo());
			JSoArry.add(PaymentReq);
		//	Map<String, Object> WalletDetails = bulkRefundSqlRepo.GetWalletDeatils(Mid,BeneList.getAmount(),BeneList.getClientRefNo());
		/*
		 * log.info(WalletDetails +"Merchant Wallet Details{}{}{}{}:::::::::;; ");
		 * IsApplicable=WalletDetails.get("IsApplicable").toString();
		 * ErrMsg=WalletDetails.get("ErroMsg").toString();
		 * ErrCode=WalletDetails.get("ErroCode").toString();
		 */
		}
		JSONObject Responst = new JSONObject();

 if(IsApplicable.equalsIgnoreCase("Y") && ErrCode.equalsIgnoreCase("0000")) {
		Reqreasy1.put("paymentRequests", JSoArry.toString());
		String SignKey = Createsignature2(Reqreasy1.toString(), Key);
		String KeyS = generateSignature(SignKey + Checsum, Key);
		// String cre1= Createsignature1(Reqreasy.toString());
		String Obj1 = ob.writeValueAsString(JSoArry);

		Reqreasy.put("paymentRequests", Obj1);
		Reqreasy.put("signature", KeyS);

		// String cre= Createsignature1(JSoArry.toString());
		String Obj = ob.writeValueAsString(Reqreasy);

		String ModifieDString = Obj.replaceAll("\"\\[", "[").replaceAll("\\]\"", "]");
		String res1 = ModifieDString.replace("\\", "");

		log.info("credentials:::::::::::::::cre::;;;;; " + Checsum);
		String url = "https://merchant.indusspay.com/payouts/api/v1/ind/p1/payouts";
		String Response = S2SCall.ServerToServerCallINDUSS(url, ModifieDString.replace("\\", ""));
		JSONObject Responsttomerchant = new JSONObject(Response);

		if (Responsttomerchant.has("status") && Responsttomerchant.getString("status").equalsIgnoreCase("FAILURE")) {
			Responst.put("Status", Responsttomerchant.getString("status"));
			Responst.put("ResMassage", Responsttomerchant.getString("message"));
		} else {
			Responst.put("Status", Responsttomerchant.getString("status"));
			Responst.put("ResMassage", Responsttomerchant.getString("message"));
		}
		log.info("credentials:::::::::::::::cre::;;;;; " + Response);
 }else {
	 Responst.put("Status", "Failed");
	 Responst.put("ResMassage", ErrMsg);

 }
		return Responst;

	}

	public String Createsignature(String Value, String Key) {
		StringBuilder sb = new StringBuilder();
		Class<?> thisClass = null;
		try {
			sb.append(Value.replaceAll("[^a-zA-Z0-9]", "").replace("beneList", ""));
			System.out.println(sb.toString());
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();

	}

	public String Createsignature2(String Value, String Key) {
		StringBuilder sb = new StringBuilder();
		Class<?> thisClass = null;
		try {
			sb.append(Value.replaceAll("[^a-zA-Z0-9]", "").replace("paymentRequests", ""));
			System.out.println(sb.toString());
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();

	}

	public String Createsignature1(String Value) {
		StringBuilder sb = new StringBuilder();
		Class<?> thisClass = null;
		try {
			sb.append(Value.replaceAll("[^a-zA-Z0-9]", ""));
			System.out.println(sb.toString());
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();

	}

	public static void main(String[] args) throws JsonSyntaxException

	{
		String beneList = "{\r\n" + "  \"beneList\": [\r\n" + "    {\r\n" + "      \"name\": \"Jackson\",\r\n"
				+ "      \"email\": \"jackson@indusspay.com\",\r\n" + "      \"mobileNumber\": \"989899898\",\r\n"
				+ "      \"beneName\": \"Jackson\",\r\n" + "      \"beneAccountNumber\": \"1002301044741\",\r\n"
				+ "      \"beneIfsc\": \"CNRB0001007\",\r\n" + "      \"bankName\": \"INDUSS BANK\"\r\n" + "    },\r\n"
				+ "    {\r\n" + "      \"name\": \"Alis\",\r\n" + "      \"email\": \"tony@gmail.com\",\r\n"
				+ "      \"mobileNumber\": \"9872343210\",\r\n" + "      \"beneName\": \"Alis Tony\",\r\n"
				+ "      \"beneAccountNumber\": \"20348938148\",\r\n" + "      \"beneIfsc\": \"SBIN0016491\",\r\n"
				+ "      \"bankName\": \"SBI\"\r\n" + "    }\r\n" + "  ]\r\n" + "}";

		JSONObject Reqq = new JSONObject(beneList);
		Gson gson = new Gson();

		try {

			BeneFiledsDto BeneDetails = gson.fromJson(Reqq.toString(), BeneFiledsDto.class);
			// BeneDetails.setBeneListDto(BeneDetails.getBeneListDto());
			System.out.println(BeneDetails.getBeneListDto());
		} catch (JsonSyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println(beneList);

		// TODO Auto-generated method stub
		JSONObject json = new JSONObject();

		json.put("userId", "");

		JSONObject que1 = new JSONObject();
		que1.put("questionId", "");
		que1.put("groupId", "");
		que1.put("answer", "");
		JSONObject que2 = new JSONObject();
		que2.put("questionId", "");
		que2.put("groupId", "");
		que2.put("answer", "");
		JSONObject que3 = new JSONObject();
		que3.put("questionId", "");
		que3.put("groupId", "");
		que3.put("answer", "");

		JSONArray jsonArray = new JSONArray();
		jsonArray.put(que1);
		jsonArray.put(que2);
		jsonArray.put(que3);

		json.put("answers", jsonArray);

		
		String randomNumber = "REW" + String.valueOf((int)(Math.random() * 1000000));
		System.out.println("Answer request : " + json.toString());

	}

	public String generateSignature(String concatenatedString, String checksumkey) throws Exception {
		try {
			String inputString = concatenatedString + "|" + checksumkey;
			byte[] decoded = Base64.getDecoder().decode(checksumkey.getBytes());
			Key key = new SecretKeySpec(decoded, "AES");
			Cipher c = Cipher.getInstance("AES");
			c.init(Cipher.ENCRYPT_MODE, key);
			byte[] encVal = c.doFinal(inputString.getBytes());
			return Base64.getEncoder().encodeToString(encVal);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
