package com.crm.services;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Service;

import com.crm.Controller.MerchantPayinController;

@Service
public class MerchantPayinService {

	private static Logger log = LoggerFactory.getLogger(MerchantPayinService.class);

	@Autowired
	private JdbcTemplate JdbcTemplate;
	
	public Map<String, Object> insertMerchant_payoutmdrRecord(String merchant_id, String sp_id, String mop, String aggr_mdr_Type,
			String aggr_mdr, String base_mdr_Type, String base_mdr, String start_date, String end_date, String mid,
			String tid, String min_amt, String max_amt) {
		String responseVal =null;
		Map<String, Object> Msg = null ;
		JSONObject js1 = new JSONObject();
		List<SqlParameter> prmtrsList = new ArrayList<SqlParameter>();
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		

		log.info("ParmVlaues::::::::::::::: " + prmtrsList);

		Map<String,Object> resultData;
		try {
			log.info("inside the insert payout mdr in db::::::::::::::: " + prmtrsList);

		resultData=JdbcTemplate.call(connection -> {
			CallableStatement callableStatement = connection
					.prepareCall("{call pro_insert_merchant_payout_mdr(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			callableStatement.setString(1, merchant_id);
			callableStatement.setString(2, sp_id);
			callableStatement.setString(3, mop);
			callableStatement.setString(4, aggr_mdr_Type);
			callableStatement.setString(5, aggr_mdr);
			callableStatement.setString(6, base_mdr_Type);
			callableStatement.setString(7, base_mdr);
			callableStatement.setString(8, start_date);
			callableStatement.setString(9, end_date);
			callableStatement.setString(10, mid);
			callableStatement.setString(11, tid);
			callableStatement.setString(12, min_amt);
			callableStatement.setString(13, max_amt);
			

			return callableStatement;
}, prmtrsList);
		if(!resultData.isEmpty()) {
			Object obj=resultData.get("#result-set-1");
			 js1.put("Status", "Success");
			 js1.put("Message", "Inserted Successfully");
			 js1.put("data", obj);
		 }else {
			 js1.put("Status", "Error");
			 js1.put("Message", "Failed to insert");
		 }
		}catch (Exception e) {
			e.printStackTrace();
		}
	Msg = js1.toMap();
	return Msg;
		}

	public String DeleteByID(String Id) {
		log.info("DeleteByID::"+Id);

		String responseVal = null;
		List<SqlParameter> prmtrsList = new ArrayList<SqlParameter>();
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		Map<String, Object> resultData ;
		try {
			log.info("DeleteByID in try ::"+Id);

		resultData = JdbcTemplate.call(connection -> {
				CallableStatement callableStatement = connection
						.prepareCall("Delete from tbl_merchant_payout_mdr where Id='"+Id+"';");
//				callableStatement.setString(1, Id);
				
				return callableStatement;
		}, prmtrsList);
		
		
		System.out.print("resultData::::::::::::::: " + resultData);
		Object val = resultData.get("#inserted-set-1");
		System.out.print("val::::::::::::::: " + val);
		String s2 = String.valueOf(val);
		System.out.print("s2::::::::::::::: " + s2);

		if (s2 != "0") {
			responseVal = "success";
		} else {
			responseVal = "fail";
		}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
		return responseVal;


	}

	public Map<String, Object> getPayoutmdr() {
		List<SqlParameter> prmtrsList = new ArrayList<SqlParameter>();
		prmtrsList.add(new SqlParameter(Types.VARCHAR));
		Map<String, Object> Msg = null ;
		JSONObject js1 = new JSONObject();
		JSONArray array = new JSONArray();
		Map<String, Object> role1Data =    JdbcTemplate.call(connection -> {
				CallableStatement callableStatement = connection
						.prepareCall("{call pro_gettbl_merchant_payout_mdr()}");

				return callableStatement;
	}, prmtrsList);
				if(!role1Data.isEmpty()) {
				Object obj=role1Data.get("#result-set-1");
				 js1.put("Status", "Success");
				 js1.put("Message", "Data Found");
				 js1.put("data", obj);
			 }else {
				 js1.put("Status", "Error");
				 js1.put("Message", "Data Not Found");
			 }
		Msg = js1.toMap();
		return Msg;
	}

	

}
