package com.crm.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import javax.persistence.Tuple;
import javax.persistence.TupleElement;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import com.crm.Repository.BulkRefundBatchDetailRepo;
import com.crm.Repository.BulkRefundRepo;
import com.crm.Repository.BulkRefundSqlRepo;
import com.crm.Repository.MerchantMdrRepo;
import com.crm.Repository.MerchantRepository;
import com.crm.Repository.RefundAmountInsertRepo;
import com.crm.Repository.RefundRepo;
import com.crm.Repository.RefundTransactionUpdateRepo;
import com.crm.config.VassFileReader;
import com.crm.dto.BulkBatchSql;
import com.crm.helper.FileUploadHelper;

import com.crm.model.BulkRefund;
import com.crm.model.BulkRefundBatchDetails;
import com.crm.model.BulkRefundSql;
import com.crm.model.RefundAmtRequest;
import com.crm.model.RefundTransaction;
import com.crm.model.RefundTrasactionMod;
import com.crm.util.GeneralUtil;
import com.crm.util.GenerateRandom;
import com.google.gson.Gson;

@Service
public class RefundTransactionService {	
	static Logger log = LoggerFactory.getLogger(RefundTransactionService.class);
	public static String uploadDirectory = "/home/BulkRefundsFiles/BulkRefund";
	public static String uploadDirectoryForDownload = "/home/BulkRefundsFiles/ManualDownload";
	public static String uploadDirectoryForUpload = "/home/BulkRefundsFiles/ManualUpload";
	@Autowired
	private com.crm.Repository.RefundTransactionRepo refundTransactionRepo;
	@Autowired
	RefundAmountInsertRepo refundAmtInsertRepo;

	@Autowired
	RefundTransactionUpdateRepo trasactionUpdateRepo;
	
	@Autowired
	BulkRefundBatchDetailRepo bulkRefundBatchDetailRepo;
	
	@Autowired
	BulkRefundRepo bulkRefundRepo;
	
	@Autowired
	BulkRefundSqlRepo bulkRefundSqlRepo;
	
	@Autowired
	private MerchantMdrRepo merchantMdrRepo;

	@Autowired
	MerchantRepository merchantrepo;
	@Autowired
	RefundRepo refundRepo;
	@Autowired
	MongoTemplate mongoTemplate;

	public List<Object> createRefundAmtt(JSONArray jsonarray) {
		String jsonArray = null;
		List<Object> ListResponse = new ArrayList<Object>();
		
			for (int i = 0; i < jsonarray.length(); i++) {
				JSONObject jsonobject = jsonarray.getJSONObject(i);
				String TransId = jsonobject.getString("TransId");
				Double ReqRefundAmt = jsonobject.getDouble("RefundAmt");
				String Merchant_Id = jsonobject.getString("Merchant_Id");
	//			-----
				String refundRequestId = GenerateRandom.randomAlphaNumeric(14);
				BulkRefund bulkrefund = new BulkRefund();
				bulkrefund.setMerchantId(Merchant_Id);
				bulkrefund.setTxnId(TransId);
				bulkrefund.setRefundAmount(ReqRefundAmt);
				bulkrefund.setMerchantRefId(refundRequestId);
	//			JSONObject resp = ValidateTxnId(bulkrefund);
				JSONObject resp = refundOldModule(bulkrefund);
	//			--------
	
				ListResponse.add(resp.toMap());
			}
		
		Gson gson = new Gson();
		jsonArray = gson.toJson(ListResponse);
		return ListResponse;

	}

	public Map<String,Object> getRefundTransactionList(String id, String txnId, String merchantId, String fromDate, String toDate,
			String bankId, String custMail, String custMobile ) throws ParseException {
		Map<String, Object> Msg = null ;
		String strDate = "";
		String endDate = "";
		JSONObject js1 = new JSONObject();
		JSONArray array = new JSONArray();
		String Query = "";
		 List<Object[]> resultdata = merchantrepo.refundAmtList(merchantId, fromDate, toDate, id, bankId, custMobile, custMail, txnId);
		 if(!resultdata.isEmpty()) {
			 for(Object[] obj:resultdata) {
				 JSONObject item = new JSONObject();
				 SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				 String string  = dateFormat.format(obj[6]);
				 item.put("TransactionId", obj[0].toString());
				 item.put("TransactionAmount", obj[1]);
				 item.put("MerchantId", obj[2]);
				 item.put("TxnId", obj[3]);
				 item.put("ReconStatus", obj[7]);
				 item.put("BankId", obj[9]);
				 item.put("BalanceAmount", obj[10]);
				 item.put("TransactionDate", string);
				 item.put("ServiceRRN", obj[8]);
				 item.put("ProcessId", obj[11]);
				 item.put("MobileNo", obj[4]);
				 item.put("EmailId", obj[5]);
				 array.put(item);
			 }

			 js1.put("Status", "Success");
			 js1.put("Message", "Data Found");
			 js1.put("data", array);
		 }else {
			 js1.put("Status", "Error");
			 js1.put("Message", "No Data found");
		 }
		Msg = js1.toMap();
		return Msg;
	}
	
	
public Map<String,Object> bulkRefundAmtCreate(MultipartFile file, String userId) {
	List<Object> ListResponse = new ArrayList<Object>();
	String FilexlName = file.getOriginalFilename();
	File filexl = new File(FilexlName);
	String jsonArray = null;
	StringBuilder filesname = new StringBuilder();
	JSONObject js1 = new JSONObject();
	Map<Integer, String> validationMessageMap = null;
	Map<String, Object> Msg = null ;
	if(file !=null) {
		String extension = FilexlName.substring(FilexlName.lastIndexOf(".") + 1);
		System.out.print ("File  extension :::  " + extension);
		System.out.print("File name=" + FilexlName);
		
		if (extension.equalsIgnoreCase("xlsx") || extension.equalsIgnoreCase("xls")) {
			String filesPath = uploadDirectory + "/" ;
			File newFile = new File(uploadDirectory + "/" + FilexlName );
			
			if (!newFile.exists()) {            
				newFile.mkdirs();            
	        }
			if(!("").equals(filesPath)){
				
				if (newFile.exists()) {
					newFile.delete();
				}
			}
			Path path = Paths.get(filesPath,FilexlName);
			String filesnames = filesPath + FilexlName;
			try {
				Files.write(path, file.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
				log.info("File Write Error   " + filesnames);
			}
			List<List<String>> rows = FileUploadHelper.readDataFromXLS(newFile);
			if (rows != null) {
				int Datasize = rows.size();
				double refundAmount = 0.0;
				int transactionCount = 0;
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu/MM/dd HH:mm:ss");
				LocalDateTime now = LocalDateTime.now();
				String prDate = dtf.format(now);
				List<BulkRefundSql> bulkRefundList = new ArrayList<>();
				validationMessageMap = new HashMap<Integer, String>();
				Random random = new Random();
				String id = String.format("%04d", random.nextInt(10000));
				
				for (int i = 1; i < Datasize; i++) {					
					BulkRefundSql bulkrefund = new BulkRefundSql();				
//					BulkRefund bulkrefundList = getBulkRefundFileRecord(rows.get(i), id);
					List<String> dataRows = rows.get(i);
					if (dataRows.size() >= 2) {						
						if (dataRows.get(0).contains("'")) {
							bulkrefund.setTxn_id(dataRows.get(0).substring(0));
							
						} else {
							bulkrefund.setTxn_id(dataRows.get(0));
						}
						double RefundAmt = Double.parseDouble(dataRows.get(1));
						String refundRequestId = GenerateRandom.randomAlphaNumeric(14);
//						RefundTrasactionMod merchantDetails = trasactionUpdateRepo.findBytxnId(bulkrefund.getTxnId());
						String MerchantId = merchantrepo.findtransctionById(bulkrefund.getTxn_id());
						log.info("transaction   " + MerchantId);
						bulkrefund.setBatch_id(id);
						bulkrefund.setAmount(RefundAmt);
						bulkrefund.setStatus("Uploaded");
						bulkrefund.setMerchantRefId(refundRequestId);
						bulkrefund.setIs_deleted(0);
						bulkrefund.setMerchantId(MerchantId);
						bulkRefundList.add(bulkrefund);
						
					}
//					String validationResponse = isValid(bulkrefund);
					JSONObject resp = refundBulkModule(bulkrefund);
					
					if (resp.getString("error_code").equalsIgnoreCase("RF000")) {
						transactionCount++;
						refundAmount +=(bulkrefund.getAmount());
						String errorStatus = "";
						bulkrefund.setError_message(resp.getString("respMessage"));
						
//						validationMessageMap.put(i, "Refund Raised Successfully");
						String srno = String.valueOf(i);
						js1.put(srno, resp.getString("respMessage"));
					} else {
						bulkrefund.setError_message(resp.getString("respMessage"));
						String srno = String.valueOf(i);
						js1.put(srno, resp.getString("respMessage"));
					}
				}
				if (transactionCount > 0) {
					
//					BulkBatchSql bulkRefundBatch = new BulkBatchSql();
//					bulkRefundBatch.setBatch_id(id);
//					bulkRefundBatch.setMerchant_id(userId);
//					bulkRefundBatch.setRefund_amount(refundAmount);
//					bulkRefundBatch.setTransaction_count(transactionCount);
//					bulkRefundBatch.setUploadedOn(prDate);
//					bulkRefundBatch.setModifiedOn(prDate);
//					bulkRefundBatch.setStatus("Processed");
//					bulkRefundBatch.setIs_deleted(0);
//					bulkRefundBatch.setFile_path(filesPath);
//					bulkRefundRepo.insert(bulkRefundList);
//					bulkRefundBatchDetailRepo.insert(bulkRefundBatch);
					merchantrepo.updateTableRecords(id, userId, transactionCount, refundAmount, "Processed", 0, filesnames );
					log.info("update Batch   " + id + userId + transactionCount + refundAmount  +  "Processed" + filesnames);
					bulkRefundSqlRepo.saveAll(bulkRefundList);
					log.info("update Bulk List " + bulkRefundList);
					
				} else {
					if (validationMessageMap.isEmpty()) {
						String srno = String.valueOf(0);
						js1.put(srno,"No Data found");
					}else {
						String srno = String.valueOf(0);
						js1.put(srno ,"File Contains Some Invalid Data!!!");
				}

			}
		}
		
		}
	
//		ListResponse.add(validationMessageMap);
//		Gson gson = new Gson();
//		 jsonArray = gson.toJson(ListResponse);
		Msg=js1.toMap();
		  
	}
	return Msg;

	}

	private String isValid(BulkRefund bulkRefund) {
		JSONObject resprefund = new JSONObject();
		String response = "1";
		if (bulkRefund == null)
			response = "Transaction Id Or Refund Amount is missing";
		else if (bulkRefund.getBatchId() == null || bulkRefund.getBatchId().equals(""))
			response = "Invalid Filename";
		else if (bulkRefund.getTxnId() == null || bulkRefund.getTxnId().equals(""))
			response = "Invalid Transaction Id";
		else if (bulkRefund.getRefundAmount() == null || bulkRefund.getRefundAmount().equals(""))
			response = "Invalid Refund Amount";
		else {
			try {
				resprefund = ValidateTxnId(bulkRefund);
				String err =  resprefund.getString("error_code");
				if (err.equals("RF000")) {
					response = "1";
				}else {
					response = resprefund.getString("respMessage");
				}
			} catch (Exception e) {
				response = "problem occurred while validating transaction";
				e.printStackTrace();
			}
		}
		return response;
	}
	public JSONObject ValidateTxnId (BulkRefund bulkRefund) {
		String TransId= bulkRefund.getTxnId();
		String Merchant_Id = bulkRefund.getMerchantId();
		Double ReqRefundAmt = bulkRefund.getRefundAmount();
		RefundAmtRequest refunAmt = new RefundAmtRequest();
		String error_code = "";
		String respMessage = "";
		String refundType = "";
		RefundTrasactionMod transaction = trasactionUpdateRepo.findBytxnIdandMerchantId(TransId, Merchant_Id);

		if (transaction != null) {
			RefundAmtRequest sdf = refundRepo.getRefundtotal(TransId, Merchant_Id);
			Double refundamtmongo = 0.00;

			String remarks = "";

			if (sdf != null) {
				refundamtmongo = sdf.getTotal();
			} else {
				refundamtmongo = 0.00;
			}

			String tamt = transaction.getAmount();
			double transactionAmt = Double.parseDouble(tamt);

			double remainingAmt = transactionAmt - refundamtmongo;
			double RefundTotalAmt = refundamtmongo + ReqRefundAmt;
			String refundRequestId = "";

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now().plusDays(1);
			LocalDateTime now1 = LocalDateTime.now();

			String prDate = dtf.format(now);
			String prDate1 = dtf.format(now1);
			refundRequestId = bulkRefund.getMerchantRefId();
			refunAmt.setTransId(TransId);
			refunAmt.setMerchant_Id(Merchant_Id);
			refunAmt.setRefundAmt(ReqRefundAmt);
			refunAmt.setIs_Processed("N");
			refunAmt.setRefund_Process_date(prDate);
			refunAmt.setAdded_By(Merchant_Id);
			refunAmt.setAdded_On(prDate1);
			refunAmt.setModify_By(Merchant_Id);
			refunAmt.setModified_On(prDate1);
			refunAmt.setRefund_RequestId(refundRequestId);

			String strdate = transaction.getDateTime();
			Date date1 = null;
			try {
				date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(strdate);
			} catch (ParseException e1) {
				
				e1.printStackTrace();
			}
			Calendar c1 = Calendar.getInstance();
			Date today = c1.getTime();
			int difference = date1.getMonth() - today.getMonth();

			String reConStatus = transaction.getReconstatus();

			if (difference <= -6) {
				error_code = "RF009";
				respMessage = "You can not initiate refund for more than 6 month old transaction.";
				refundType = "NA";
			} else if (difference <= -12) {
				error_code = "RF009";
				respMessage = "You can not initiate refund for more than 12 month old transaction.";
				refundType = "NA";

			} else if ((transaction.getReconstatus()).equals("AFR")
					|| (transaction.getReconstatus()).equals("FR")) {
				error_code = "RF003";
				respMessage = "This Trnsaction is already full refunded.";
				refundType = "NA";
			} else if (reConStatus.equals("NRNS") || reConStatus.equals("RNS") || reConStatus.equals("RS")
					|| reConStatus.equals("PR")) {
				refunAmt.setResp_message(null);
				refunAmt.setRefund_Status("New");
				refunAmt.setRefund_Type("Manual");
				if (transactionAmt > RefundTotalAmt) {
					refundType = "PR";
					remarks = "Partial Refund Initiated";
					refunAmt.setRefund_Type(refundType);
					refunAmt.setRemark(remarks);
					refundAmtInsertRepo.insert(refunAmt);
					RefundTrasactionMod updtTrans = new RefundTrasactionMod();
					updtTrans = trasactionUpdateRepo.findByatrn(TransId, Merchant_Id);
					error_code = "RF000";
					respMessage = "Refund Request Raised Successfully";
					updtTrans.setErrorCode(error_code);
					updtTrans.setRespMessage(respMessage);
					updtTrans.setReconstatus(refundType);
					refundRepo.updateTransaction(updtTrans, TransId);
				} else if (transactionAmt == RefundTotalAmt) {
					refundType = "FR";
					remarks = "Full Refund Initiated";
					refunAmt.setRefund_Type(refundType);
					refunAmt.setRemark(remarks);
					refundAmtInsertRepo.insert(refunAmt);
					RefundTrasactionMod updtTrans = new RefundTrasactionMod();
					updtTrans = trasactionUpdateRepo.findByatrn(TransId, Merchant_Id);
					error_code = "RF000";
					respMessage = "Refund Request Raised Successfully";
					updtTrans.setErrorCode(error_code);
					updtTrans.setRespMessage(respMessage);
					updtTrans.setReconstatus(refundType);
					refundRepo.updateTransaction(updtTrans, TransId);
				} else if (RefundTotalAmt > transactionAmt) {

					error_code = "RF005";
					respMessage = "Refund amount is greater than txn amount. max refund amount is  "
							+ transactionAmt + "  Remaining Balance Amt " + remainingAmt;
					refundType = "NA";

				} else {
					error_code = "RF007";
					respMessage = "Refund RequestId is Already Exist it should be unique for every Refund Request";
					refundType = "NA";
				}
			}

			
			//-------- for shedular micro service-------
			if (error_code.equals("RF000")) {
				JSONObject requestData = new JSONObject();
				requestData.put("txnId", TransId);
				requestData.put("refundAmount", ReqRefundAmt);
				requestData.put("addedBy", "admin");
				requestData.put("refundRequestId", refundRequestId);
				if (transaction != null) {
					String merchantId = transaction.getMerchantId();
					String encryptkey = null;
					List<Object[]> merchantdet = merchantrepo.findByMerchant(merchantId);
					if (merchantdet != null && merchantdet.size() > 0) {
						for (Object[] obj : merchantdet) {
							encryptkey = ((String) obj[17]);
						}

					}

					String encData = null;
					String pspclMerchantId = "";
					VassFileReader vasread = new VassFileReader();
					try {

						pspclMerchantId = vasread.getPropertyValue("pspclId");

					} catch (IOException e) {

						e.printStackTrace();
					}
					GeneralUtil generalUtil = new GeneralUtil();
						if (pspclMerchantId.equals(merchantId)) {					
							encData = generalUtil.encrypt(encryptkey, encryptkey, requestData.toString());
						} else if (encryptkey.length() == 8 || encryptkey.length() == 16) {
							encData = generalUtil.getEncData(requestData.toString(), encryptkey);
						} else if (encryptkey.length() == 0 || encryptkey == null){
							encData = generalUtil.encrypt(encryptkey, encryptkey.substring(0, 16), requestData.toString());
						}else{
							encData = generalUtil.encrypt(encryptkey, encryptkey.substring(0, 16), requestData.toString());
						}	

					JSONObject encyptData = new JSONObject();
					encyptData.put("merchantId", merchantId);
					encyptData.put("encData", encData);
					System.out.println("encyptRequestData" + encyptData);
				}
			}
		} else {
			error_code = "RF001";
			respMessage = "Transaction Id not found";
			refundType = "NA";
		}
		JSONObject resp = new JSONObject();
		resp.put("error_code", error_code);
		resp.put("respMessage", respMessage);
		resp.put("refundType", refundType);
		resp.put("MerchantId", Merchant_Id);
		resp.put("Id", TransId);
		return resp;
	}
	
	private JSONObject refundOldModule(BulkRefund bulkRefund) {
		
		String TransId= bulkRefund.getTxnId();
		String Merchant_Id = bulkRefund.getMerchantId();
		Double ReqRefundAmt = bulkRefund.getRefundAmount();
		String refundRequestId = bulkRefund.getMerchantRefId();
		RefundAmtRequest refunAmt = new RefundAmtRequest();
		String error_code = "";
		String respMessage = "";
		String refundType = "";
		
		JSONObject requestData = new JSONObject();
		requestData.put("txnId", TransId);
		requestData.put("refundAmount", ReqRefundAmt);
		requestData.put("addedBy", Merchant_Id);
		requestData.put("refundRequestId", refundRequestId);
		String transaction = merchantrepo.fintransction(TransId, Merchant_Id);
		log.info("transaction   " + transaction);
		if (transaction != null) {
//			String merchantId = transaction.getMerchantId();
			String encryptkey = null;
			List<Object[]> merchantdet = merchantrepo.findByMerchant(Merchant_Id);
			if (merchantdet != null && merchantdet.size() > 0) {
				for (Object[] obj : merchantdet) {
					encryptkey = ((String) obj[17]);
				}

			}

			String encData = null;
			String pspclMerchantId = "";
			VassFileReader vasread = new VassFileReader();
			try {

				pspclMerchantId = vasread.getPropertyValue("pspclId");
				log.info("pspclMerchantId  " + pspclMerchantId);

			} catch (IOException e) {

				e.printStackTrace();
			}
			GeneralUtil generalUtil = new GeneralUtil();
//				if (pspclMerchantId.equals(Merchant_Id)) {					
//					encData = generalUtil.encrypt(encryptkey, encryptkey, requestData.toString());
//				} else 
					if (encryptkey.length() == 8 || encryptkey.length() == 16) {
					encData = generalUtil.getEncData(requestData.toString(), encryptkey);
				} else if (encryptkey.length() == 0 || encryptkey == null){
					encData = generalUtil.encrypt(encryptkey, encryptkey.substring(0, 16), requestData.toString());
				}else{
					encData = generalUtil.encrypt(encryptkey, encryptkey.substring(0, 16), requestData.toString());
				}	
				
			JSONObject encyptData = new JSONObject();
			encyptData.put("merchantId", Merchant_Id);
			encyptData.put("encData", encData);
			System.out.println("encyptRequestData" + encyptData);
			String propertyValue = null;
			
			try {
				propertyValue = vasread.getPropertyValue("refundrequestURL");
				log.info("URL For Refund  " + propertyValue);
			} catch (IOException e) {
				log.error(e.getMessage(), e);
				log.info("error message" + e.getMessage());
			}

			String refundRequestCall = null;
			if (propertyValue != null) {
				refundRequestCall = this.refundRequestCall(propertyValue, encyptData.toString());
				log.info("refundRequestCall response " + refundRequestCall);
			}
			
			String respCode=null;

			if (refundRequestCall != null) {
				try {
					JSONObject responseData = new JSONObject(refundRequestCall);
					respCode = (String) responseData.get("resp_code");
					error_code =respCode;
					if (respCode.equals("RF000")) {

						respMessage="Refund initiated successfully";
						refundType = (String) responseData.get("refund_type_status");

					} else {

						respMessage=(String) responseData.get("resp_message");
						refundType = (String) responseData.get("refund_type_status");
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					log.error(e.getMessage(), e.getMessage());
				}
			}else {
//				 String message = (String) responseData.get("resp_message");
				respMessage="Unable to raise a refund";
				refundType = "NA";
			}			
		}else {
			respMessage="Transaction Not Found In Given Transaction Id";
			refundType = "NA";
		}
		
		JSONObject resp = new JSONObject();
		resp.put("error_code", error_code);
		resp.put("respMessage", respMessage);
		resp.put("refundType", refundType);
		resp.put("MerchantId", Merchant_Id);
		resp.put("Id", transaction);
		return resp;
		
	}
	
private JSONObject refundBulkModule(BulkRefundSql bulkRefund) {
		
		String TransId= bulkRefund.getTxn_id();
		String Merchant_Id = bulkRefund.getMerchantId();
		Double ReqRefundAmt = bulkRefund.getAmount();
		String refundRequestId = bulkRefund.getMerchantRefId();
		RefundAmtRequest refunAmt = new RefundAmtRequest();
		String error_code = "";
		String respMessage = "";
		String refundType = "";
		
		JSONObject requestData = new JSONObject();
		requestData.put("txnId", TransId);
		requestData.put("refundAmount", ReqRefundAmt);
		requestData.put("addedBy", Merchant_Id);
		requestData.put("refundRequestId", refundRequestId);
		String transaction = merchantrepo.fintransction(TransId, Merchant_Id);
		log.info("transaction   " + transaction);
		if (transaction != null) {
//			String merchantId = transaction.getMerchantId();
			String encryptkey = null;
			List<Object[]> merchantdet = merchantrepo.findByMerchant(Merchant_Id);
			if (merchantdet != null && merchantdet.size() > 0) {
				for (Object[] obj : merchantdet) {
					encryptkey = ((String) obj[17]);
				}

			}

			String encData = null;
			String pspclMerchantId = "";
			VassFileReader vasread = new VassFileReader();
			try {

				pspclMerchantId = vasread.getPropertyValue("pspclId");
				log.info("pspclMerchantId  " + pspclMerchantId);

			} catch (IOException e) {

				e.printStackTrace();
			}
			GeneralUtil generalUtil = new GeneralUtil();
//				if (pspclMerchantId.equals(Merchant_Id)) {					
//					encData = generalUtil.encrypt(encryptkey, encryptkey, requestData.toString());
//				} else 
					if (encryptkey.length() == 8 || encryptkey.length() == 16) {
					encData = generalUtil.getEncData(requestData.toString(), encryptkey);
				} else if (encryptkey.length() == 0 || encryptkey == null){
					encData = generalUtil.encrypt(encryptkey, encryptkey.substring(0, 16), requestData.toString());
				}else{
					encData = generalUtil.encrypt(encryptkey, encryptkey.substring(0, 16), requestData.toString());
				}	
				
			JSONObject encyptData = new JSONObject();
			encyptData.put("merchantId", Merchant_Id);
			encyptData.put("encData", encData);
			System.out.println("encyptRequestData" + encyptData);
			String propertyValue = null;
			
			try {
				propertyValue = vasread.getPropertyValue("refundrequestURL");
				log.info("URL For Refund  " + propertyValue);
			} catch (IOException e) {
				log.error(e.getMessage(), e);
				log.info("error message" + e.getMessage());
			}

			String refundRequestCall = null;
			if (propertyValue != null) {
				refundRequestCall = this.refundRequestCall(propertyValue, encyptData.toString());
				log.info("refundRequestCall response " + refundRequestCall);
			}
			
			String respCode=null;

			if (refundRequestCall != null) {
				try {
					JSONObject responseData = new JSONObject(refundRequestCall);
					respCode = (String) responseData.get("resp_code");
					error_code =respCode;
					if (respCode.equals("RF000")) {

						respMessage="Refund initiated successfully";
						refundType = (String) responseData.get("refund_type_status");

					} else {

						respMessage=(String) responseData.get("resp_message");
						refundType = (String) responseData.get("refund_type_status");
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					log.error(e.getMessage(), e.getMessage());
				}
			}else {
//				 String message = (String) responseData.get("resp_message");
				respMessage="Unable to raise a refund";
				refundType = "NA";
			}			
		}else {
			respMessage="Transaction Not Found In Given Transaction Id";
			refundType = "NA";
		}
		
		JSONObject resp = new JSONObject();
		resp.put("error_code", error_code);
		resp.put("respMessage", respMessage);
		resp.put("refundType", refundType);
		resp.put("MerchantId", Merchant_Id);
		resp.put("Id", transaction);
		return resp;
		
	}
	
	private String refundRequestCall(String requestURL, String data) {
		String line = null;
		BufferedReader br = null;
		StringBuffer respString = null;

		System.out.println("S2SCall.java ::: secureServerCall() :: Posting URL : " + requestURL);

		try {
			URL obj = new URL(requestURL);
			// HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");
			con.addRequestProperty("Content-Type", "application/json;charset=UTF-8");
			con.addRequestProperty("Content-Length", data.getBytes().length + "");
			con.setDoOutput(true);
			con.connect();
			
			OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
			// log.info(new SimpleDateFormat("dd-MM-YYYY HH:mm:ss").format(new Date())+" :::
			// Data length :: " + data.getBytes().length);
			wr.write(data);
			wr.flush();
			System.out.println("wrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrL : " + con.getResponseCode() );

			respString = new StringBuffer();

			if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
				System.out.println("S2SCall.java ::: secureServerCall() :: HTTP OK");
				br = new BufferedReader(new InputStreamReader(con.getInputStream()));

				while ((line = br.readLine()) != null) {
					System.out.println("S2SCall.java ::: secureServerCall() :: Response : " + line);
					respString.append(line);
				}

				br.close();
				System.out.println("S2SCall.java ::: secureServerCall() :: Response : " + respString.toString());
				return respString.toString();
			}
		} catch (Exception e) {
			System.out.printf("S2SCall.java ::: secureServerCall() :: Error Occurred while Processing Request : ", e.getMessage());

		}

		return null;
	}
	
	public Map<String,Object> getRefundDownloadRecordsList(String id, String txnId, String merchantId, String fromDate, String toDate,
			String spId, String custMail, String custMobile, String count, int pageNo, int Type, String searchBy, String refundType  ) throws ParseException {
		Map<String, Object> Msg = null ;
		String strDate = "";
		String endDate = "";
		JSONObject js1 = new JSONObject();
		JSONArray array = new JSONArray();
		
		BigInteger Id = BigInteger.ZERO;
		if (!id.equals("")) {
			 Id =new BigInteger(id.toString());
		}
		
		try {
			List<Map<String, Object>> resultdata = bulkRefundSqlRepo.refundDownloadStatusList(merchantId, fromDate, toDate, Id, txnId, custMail,  custMobile, count, spId, pageNo, Type, searchBy, refundType );
				if(!resultdata.isEmpty()) {
					 js1.put("Status", "Success");
					 js1.put("Message", "Data Found");
					 js1.put("data", resultdata);
				}else {
					 js1.put("Status", "Error");
					 js1.put("Message", "No Data found");
					 js1.put("data", "");
				}
		}catch(Exception e) {
			 js1.put("Status", "Error");
			 js1.put("data", "Error Found");
			log.info("Refund Transaction Status ::::::::::::::::::{} "+e.getMessage());
		}

		Msg = js1.toMap();
		return Msg;
	}

	public Map<String, Object> getRefundTransactionStatusList(String id, String txnId, String refundId, String fromDate, String toDate,
			String refundType, String refundStatus, String count, int pageNo, int Type, String searchBy) {
		Map<String, Object> Msg = null ;
		String strDate = "";
		String endDate = "";
		JSONObject js1 = new JSONObject();
		JSONArray array = new JSONArray();	
		
		try {
			List<Map<String, Object>> resultdata = bulkRefundSqlRepo.refundTransactionStatus( fromDate, toDate, id, txnId, refundId,  refundType, refundStatus, count, pageNo, Type, searchBy );
		
			if(!resultdata.isEmpty()) {
				 js1.put("Status", "Success");
				 js1.put("Message", "Data Found");
				 js1.put("data", resultdata);
			}else {
				 js1.put("Status", "Error");
				 js1.put("Message", "No Data found");
				 js1.put("data", "");
			}
		}catch(Exception e) {
			 js1.put("Status", "Error");
			 js1.put("data", "Error Found");
			log.info("Refund Transaction Status ::::::::::::::::::{} "+e.getMessage());
		}

		Msg = js1.toMap();
		return Msg;
	}

	public Map<String, Object> getRefundDownloadFileFomList(JSONArray jsonarray) {
		JSONObject js1 = new JSONObject();
		Map<String, Object> Msg = null ;
		ArrayList< Tuple > list = new ArrayList< Tuple >();
		JSONObject js = new JSONObject();
		String UserId =  null;
		for (int i = 0; i < jsonarray.length(); i++) {
			JSONObject jsonobject = jsonarray.getJSONObject(i);
			String RefundId = jsonobject.getString("refundId");
			 UserId = jsonobject.getString("userId");
			try {
				List<Tuple> refundinfo =bulkRefundSqlRepo.getrefundInfo(RefundId);
				if (!refundinfo.isEmpty()) {
					list.addAll(refundinfo);
				}else {
					js1.put("Status", "Error");
					js1.put("Message", "No Data found");
				}
			}catch(Exception e) {
				log.info("Refund Manual File Download  ::::::::::::::::::{} "+e.getMessage());
			}
		}
			js = createxlsxFile(list, UserId);
				if(js.get("Status").equals("Success")) {
						String filename =js.getString("FileName");
						String fileLocation = js.getString("FilePath");
						
						js1.put("Status", "Success");
						js1.put("Message", js.getString("Message"));
						js1.put("FileName", filename);
						js1.put("FilePath", fileLocation);
			
					}else {
					log.info("Refund Manual Download :::else:::::::::::::::{} ");
					js1.put("Status", "Error");
					js1.put("Message", "File Not Generated");
			}
		Msg = js1.toMap();
		return Msg;
	}
	
	public JSONObject createxlsxFile(List<Tuple> list, String userId) {
		JSONObject js1 = new JSONObject();
		StringBuilder filesname = new StringBuilder();
		XSSFWorkbook workBook = new XSSFWorkbook();
		XSSFSheet sheet = workBook.createSheet("data");
		XSSFRow hrow = sheet.createRow(0);
		XSSFRow vrow = sheet.createRow(1);
		int rowCount = 0;
		int rowCounthead = 0;        

		for(Tuple value : list) {
			int columnCount = -1;
            int columnCounthead = -1;
			Row row = sheet.createRow(++rowCount);
			Row rowhead = sheet.createRow(rowCounthead);
			List<TupleElement<?>> elements = value.getElements();
		    for (TupleElement<?> element : elements ) {
		    	Cell vcell = row.createCell(++columnCount);
		        vcell.setCellValue("" + value.get(element.getAlias()));
		        Cell hcell = rowhead.createCell(++columnCounthead);
		        hcell.setCellValue(element.getAlias());
		    }

		}
		
		Date date = Calendar.getInstance().getTime();  
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
        String strDate = dateFormat.format(date);  
        String strDatewospc = strDate.replaceAll("\\s", "");
        String strDatewosp = strDatewospc.replaceAll("[-:]*", "");
		String fileOrgname = (strDatewosp +".xlsx");
		String filesname1 = uploadDirectoryForDownload + "/" + userId +"/";
		fileOrgname= fileOrgname.replaceAll("\\s", "");
		String nameFileconvert =  "Manual_Refund"+"_"+fileOrgname;
		File file = new File(filesname1);
		if (!file.exists()) {            
            file.mkdirs();            
        }	
		try {
			Path path = Paths.get(filesname1,nameFileconvert);
			filesname.append(nameFileconvert);
			String DocRef = filesname1 + nameFileconvert;
			FileOutputStream outputStream = new FileOutputStream(DocRef);
			workBook.write(outputStream);
			log.info("File Create :::Name:::::::::::::::{} "+ DocRef);
			js1.put("Status", "Success");
			js1.put("Message", "File Created and Uploaded ");
			js1.put("FilePath", DocRef);
			js1.put("FileName", nameFileconvert);
		} catch (Exception e) {
			log.info("Error in File Create :::Name:::::::::::::::{} "+ e.getMessage());
			e.printStackTrace();
			js1.put("Status", "Error");
			js1.put("Message", "File Not Created " + e.getMessage());
		}
		
		return js1;
		
	}

	public Map<String, Object> uploadManualRefundFile(MultipartFile file, String userId, String sPID) {
		JSONObject js1 = new JSONObject();
		JSONArray array = new JSONArray();
		Map<String, Object> Msg = null ;
		String FilexlName = file.getOriginalFilename();
		File filexl = new File(FilexlName);
		boolean resultFlag = true;
		String errorTXNIds = "";
		if(file !=null) {
			String extension = FilexlName.substring(FilexlName.lastIndexOf(".") + 1);
			log.info("File Name ::    " + FilexlName);
			log.info("File Name extension  ::    " + extension);
			String filesPathtxt = uploadDirectoryForUpload + "/" ;
			File newFiletxt = new File(uploadDirectoryForUpload + "/" + FilexlName );
			
			if (!newFiletxt.exists()) {            
				newFiletxt.mkdirs();            
	        }
			if(!("").equals(filesPathtxt)){
				
				if (newFiletxt.exists()) {
					newFiletxt.delete();
				}
			}
			Path pathtxt = Paths.get(filesPathtxt,FilexlName);
			String filesnamestxt = filesPathtxt + FilexlName;
			try {
				Files.write(pathtxt, file.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
				log.info("File Write Error   " + filesnamestxt);
			}
			if (extension.equalsIgnoreCase("txt") && sPID.equalsIgnoreCase("10")) {
				JSONObject item = new JSONObject();
				try {
					log.info("Helooooooooooooo am in tryyy block ");
					// csv file containing data
//					File file1 = getFilePath(file);
					log.info("............................................." + file);
					// create BufferedReader to read csv file
					BufferedReader br = new BufferedReader(new FileReader(newFiletxt));
					String strLine = "";
					StringTokenizer st = null;
					int lineNumber = 0, tokenNumber = 0;
					strLine = br.readLine();
					
					// read comma separated file line by line
					while (strLine != null) {
						if (lineNumber == 0) {
							lineNumber++;
							continue;
						}

						// break comma separated line using ","
						st = new StringTokenizer(strLine, ",");
						Map<Integer, String> resultSet = new HashMap<Integer, String>();
						while (st.hasMoreTokens()) {
							tokenNumber++;
							System.out.println("Line # #######+" + lineNumber +" Token # " + tokenNumber + ",Token : "+ st.nextToken());
							resultSet.put(tokenNumber, st.nextToken());
						}

						String refundFlag = resultSet.get(7);
						String remark = resultSet.get(8);
						String isApproveFlag = null;
						String refundId = resultSet.get(5); // Sequence No.
						String txnId = resultSet.get(2); // Merchant Ref. No.
						String isProcessed = null;
						String refundStatus = null;
						if (refundFlag.equalsIgnoreCase("Success")) {
							isApproveFlag = "Y";
							isProcessed = "Y";
							refundStatus = "Success";
						} else {
							isApproveFlag = "N";
							isProcessed = "N";
							refundStatus = "Failed";
						}

						log.info("data updated remark=" + remark + "|isApproveFlag=" + isApproveFlag + "orderId" + txnId
								+ "|RefundId=" + refundId + "|isProcessed=" + isProcessed + "|refundStatus="
								+ refundStatus);
						String requestStatus = "";
						String txnDtO = null;
						String findByProperty = merchantrepo.findByProperty( txnId);
						if (findByProperty != null || !findByProperty.isEmpty()) {
//							txnDtO = (TblTransactionmaster) findByProperty.get(0);
							String reconStatus = findByProperty;
							if (reconStatus.equalsIgnoreCase("NRNS")) {
								requestStatus = "Success";
							} else {
								requestStatus = "Intiated";
							}
						}
						Timestamp TimeSpan = new Timestamp(new Date().getTime());
						int updateRefundStatus=bulkRefundSqlRepo.updateRefundStatusForManual(refundStatus, requestStatus,
								isApproveFlag, refundId, refundStatus, remark, userId, TimeSpan );
//						int updateRefundStatus = updateRefundStatus(refundStatus, requestStatus, isApproveFlag,
//								refundId, remark);

						if (updateRefundStatus == 0) {
							errorTXNIds = refundId + ", " + errorTXNIds;
						}
						// reset token number
						tokenNumber = 0;

					}
					br.close();
					if (errorTXNIds.equals("")) {
						String message = "File has been uploaded succesfully";
						item.put("Status", "Success");
						item.put("Message", message);
					} else {
						String message = "Error Occure while updating refund Ids "
								+ errorTXNIds.substring(0, errorTXNIds.length() - 1) + ".";
						item.put("Status", "Error");
						item.put("Message", message);
					}

				} catch (Exception e) {
					String message ="Exception while reading file: " + e.getMessage();
					log.info("Exception while reading file: " + e.getMessage());
					item.put("Status", "Error");
					item.put("Message", message);
				}
				array.put(item );
				js1.put("Status", "Success");
				js1.put("Data", array);
			}else if (extension.equalsIgnoreCase("xlsx") || extension.equalsIgnoreCase("xls")) {
				String filesPath = uploadDirectoryForUpload + "/" ;
				File newFile = new File(uploadDirectoryForUpload + "/" + FilexlName );
				
				if (!newFile.exists()) {            
					newFile.mkdirs();            
		        }
				if(!("").equals(filesPath)){
					
					if (newFile.exists()) {
						newFile.delete();
					}
				}
				Path path = Paths.get(filesPath,FilexlName);
				String filesnames = filesPath + FilexlName;
				try {
					Files.write(path, file.getBytes());
				} catch (IOException e) {
					e.printStackTrace();
					log.info("File Write Error   " + filesnames);
				}
				List<List<String>> rows = FileUploadHelper.readDataFromXLS(newFile);
				rows.remove(0);
				for (List<String> row : rows) {
					JSONObject item = new JSONObject();
					String refundRequestId = row.get(3);
					String requestStatus = row.get(4);
					String refundRemark = "Refund is Processed";
					String refundStatus = null;
					String isApproveFlag = "N";
					boolean validFile = true;
					if (refundRequestId != null && requestStatus != null) {
						if (requestStatus.equalsIgnoreCase("Failed")) {
							refundRemark = row.get(5);
							refundStatus = "Pending";
							validFile = validFile && true;
						} else if (requestStatus.equalsIgnoreCase("Success")) {
							refundStatus = "Success";
							isApproveFlag = "Y";
							validFile = validFile && true;
						} else {
							String message = "Invalid Status value in the file";
							item.put("Status", "Error");
							item.put("Message", message);
							validFile = validFile && false;
						}

						if (validFile) {
							Timestamp TimeSpan = new Timestamp(new Date().getTime());
							int updateRefundStatus=bulkRefundSqlRepo.updateRefundStatusForManual(refundStatus, requestStatus,
									isApproveFlag, refundRequestId, refundStatus, refundRemark, userId, TimeSpan );
//							int updateRefundStatus = updateRefundStatusForManual(refundStatus, requestStatus,
//									isApproveFlag, refundRequestId, refundStatus, refundRemark);

							log.info("resultFlag" + resultFlag);
							log.info("updateRefundStatus  " + updateRefundStatus);
							boolean isRefund = true;
							log.info("resultFlag " + resultFlag);
							if (updateRefundStatus == 0) {
								isRefund = false;
								errorTXNIds = refundRequestId + "," + errorTXNIds;
							}
							resultFlag = resultFlag && isRefund;

							log.info("errorTXNIds " + errorTXNIds);
							log.info("resultFlag " + resultFlag);
							if (resultFlag) {
								String message = "Record has been uploaded succesfully";
								item.put("Status", "Success");
								item.put("Message", message);
							} else {
								String message = "Error Occure while updating refund Ids "
										+ errorTXNIds.substring(0, errorTXNIds.length() - 1) + ".";
								item.put("Status", "Error");
								item.put("Message", message);
							}
						}
					} else {
						String message = "Invalid Row Data";
						item.put("Message", message);
					}
					array.put(item );
				}
				js1.put("Status", "Success");
				js1.put("Data", array);
			}
		}else {
			js1.put("Status", "Error");
			js1.put("Message", "File Not Found ");
		}
		Msg=js1.toMap();
		return Msg;
	}

	public Map<String, Object> atomRefundFile(MultipartFile file, String userId) {
		JSONObject js1 = new JSONObject();
		Map<String, Object> Msg = null ;
		String FilexlName = file.getOriginalFilename();
		File filexl = new File(FilexlName);
		JSONArray array = new JSONArray();
		try {
			if (file != null) {
				String extension = FilexlName.substring(FilexlName.lastIndexOf(".") + 1);
				log.info("File  extension :::  " + extension);
				log.info("File name=" + file.getOriginalFilename());

				if (extension.equalsIgnoreCase("xlsx") || extension.equalsIgnoreCase("xls")) {
					String filesPath = uploadDirectoryForUpload + "/" ;
					File newFile = new File(uploadDirectoryForUpload + "/" + FilexlName );
					
					if (!newFile.exists()) {            
						newFile.mkdirs();            
			        }
					if(!("").equals(filesPath)){
						
						if (newFile.exists()) {
							newFile.delete();
						}
					}
					Path path = Paths.get(filesPath,FilexlName);
					String filesnames = filesPath + FilexlName;
					try {
						Files.write(path, file.getBytes());
					} catch (IOException e) {
						e.printStackTrace();
						log.info("File Write Error   " + filesnames);
					}
					
					List<List<String>> rows = FileUploadHelper.readDataFromXLSX(newFile);
					Map<Integer, String> validationMessageMap = null;

					if (rows != null) {
						int Datasize = rows.size();
						int transactionCount = 0;
//						RefundService refundService = new RefundServiceImpl();
						validationMessageMap = new HashMap<Integer, String>();
						String atomTxnId = null, bankRefNo = null, merchantTxnId = null, refundAmount = null;
						for (int i = 1; i < Datasize; i++) {
							JSONObject item = new JSONObject();
							List<String> dataRows = rows.get(i);
							if (!dataRows.isEmpty() && dataRows.size() > 13) {
								atomTxnId = dataRows.get(2);
								merchantTxnId = dataRows.get(6);
								bankRefNo = dataRows.get(12);
								refundAmount = dataRows.get(13);
								Double ReqRefundAmt = Double.parseDouble(refundAmount);
								boolean resp = bulkRefundSqlRepo.updateTxnMaster(atomTxnId, merchantTxnId, bankRefNo, "Ok");
								if (resp) {
									String MerchantId = merchantrepo.findtransctionById(atomTxnId);
									String refundRequestId = GenerateRandom.randomAlphaNumeric(14);
									BulkRefund bulkrefund = new BulkRefund();
									bulkrefund.setMerchantId(MerchantId);
									bulkrefund.setTxnId(atomTxnId);
									bulkrefund.setRefundAmount(ReqRefundAmt);
									bulkrefund.setMerchantRefId(refundRequestId);
						//			JSONObject resp = ValidateTxnId(bulkrefund);
									JSONObject response = refundOldModule(bulkrefund);
//									String refundResponse = RefundManager.initiateRefund(merchantTxnId, refundAmount);
									if (response.get("respMessage").equals("Refund initiated successfully")) {
										bulkRefundSqlRepo.updateTxnMaster(atomTxnId, merchantTxnId, bankRefNo, "F");
									}
									String srno = String.valueOf(i);
									item.put(srno,response.get("respMessage"));
								} else {
									String srno = String.valueOf(i);
									item.put(srno, "Invalid Transaction Id");
								}
							} else {
								String srno = String.valueOf(i);
								item.put(srno, "Invalid number of columns");
							}
							array.put(item);
							js1.put("Status", "Successs");
							js1.put("Data", array);
						}
						
					} else {
						String Message= "No rows found!!!";
						js1.put("Status", "Error");
						js1.put("Message", Message);
					}
				} else {
					String Message= "Wrong File Format!!! File Format should be xlsx/xls";
					js1.put("Status", "Error");
					js1.put("Message", Message);
				}
			}
		} catch (Exception e) {
			String Message= "Error occurred while reading file" + e.getMessage();
			log.error(e.getMessage(), e);
			js1.put("Status", "Error");
			js1.put("Message", Message);
		}
		Msg=js1.toMap();
		return Msg;
	}
}
