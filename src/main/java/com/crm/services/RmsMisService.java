package com.crm.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.bson.BsonNull;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.crm.Repository.TransactionMasters;
import com.crm.dto.MerchantIdDto;
import com.crm.dto.TransactionMaster;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;

@Service
public class RmsMisService {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	private TransactionMasters TransactionMasters;

	public List<TransactionMaster> showAllGroceryItems() {
		List<com.crm.dto.TransactionMaster> itm = TransactionMasters.findAll();

		return itm;
	}

	public List<TransactionMaster> findStudentsByProperties(String From, String ToDate, String Mid, String RiskCode,
			String RiskStage, String RiskFlag) {
		final Query query = new Query();
		final List<Criteria> criteria = new ArrayList<>();
		if (From != "" && ToDate != "")
			Criteria.where("dateTime").gte(From).lte(ToDate);
		if (Mid != "") {
			criteria.add(Criteria.where("merchantId").is(Mid));
		}

		/*
		 * if (RiskCode != "") { criteria.add(Criteria.where("RiskCode").in(RiskCode));
		 * } if (RiskStage != "") {
		 * 
		 * criteria.add(Criteria.where("RiskStage").is(RiskStage)); }
		 */
		if (RiskFlag == "") {
			criteria.add(Criteria.where("rmsflag").is(RiskFlag));
		}
		System.out.print("Step 3:::::::::::::::: " + criteria);
		if (!criteria.isEmpty())
			query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
		System.out
				.print("Value 444 st step:::::::::::::::::::   " + mongoTemplate.find(query, TransactionMaster.class));

		return mongoTemplate.find(query, TransactionMaster.class);
	}

	/*
	 * public DistinctIterable<String> showAllGroceryItems1() { return
	 * mongoTemplate.getCollection("tbl_transactionmaster").distinct("merchantId",
	 * Filters.eq("rmsflag", ""), String.class); }
	 */
	public DistinctIterable<String> showAllGroceryItems1() {
		return mongoTemplate.getCollection("tbl_transactionmaster").distinct("merchantId", Filters.eq("riskFlag", "1"),
				String.class);
	}
	/*
	 * public Map<String, Object> getRiskLevelData(String Merchant_Id) {
	 * DistinctIterable<String> ds =
	 * mongoTemplate.getCollection("tbl_transactionmaster").distinct("rms_level",
	 * String.class); Query query = new Query(); JSONObject js = new JSONObject();
	 * query.addCriteria(Criteria.where("rmsflag").is("1"));
	 * System.out.print(Merchant_Id); if (!Merchant_Id.equalsIgnoreCase("")) {
	 * query.addCriteria(Criteria.where("merchantId").is(Merchant_Id)); } double i =
	 * mongoTemplate.count(query, "tbl_transactionmaster"); if (i <= 0) {
	 * js.put("error", "Merchant_Id Does not Exits"); return js.toMap(); }
	 * MongoCursor<String> results = ds.iterator(); while (results.hasNext()) {
	 * System.out.println(i); Query query2 = new Query(); String Level =
	 * results.next(); query2.addCriteria(Criteria.where("rmsflag").is("1"));
	 * query2.addCriteria(Criteria.where("rms_level").is(Level)); if
	 * (!Merchant_Id.equalsIgnoreCase("")) {
	 * query2.addCriteria(Criteria.where("merchantId").is(Merchant_Id)); } double j
	 * = mongoTemplate.count(query2, "tbl_transactionmaster"); double per = (j / i);
	 * double value = per * 100; js.put(Level, value); System.out.println(j); }
	 * return js.toMap(); }
	 *
	 */

	/*
	 * public Map<String, Object> getRiskLevelData(String Merchant_Id) {
	 * DistinctIterable<String> ds =
	 * mongoTemplate.getCollection("tbl_transactionmaster").distinct("rms_level",
	 * String.class); Query query = new Query(); JSONObject js = new JSONObject();
	 * query.addCriteria(Criteria.where("rmsflag").is("1"));
	 * System.out.print(Merchant_Id); if (!Merchant_Id.equalsIgnoreCase("")) {
	 * query.addCriteria(Criteria.where("merchantId").is(Merchant_Id)); } double i =
	 * mongoTemplate.count(query, "tbl_transactionmaster"); if (i <= 0) {
	 * js.put("error", "Merchant_Id Does not Exits"); return js.toMap(); }
	 * MongoCursor<String> results = ds.iterator(); while (results.hasNext()) {
	 * System.out.println(i); Query query2 = new Query(); String Level =
	 * results.next(); query2.addCriteria(Criteria.where("rmsflag").is("1"));
	 * query2.addCriteria(Criteria.where("rms_level").is(Level)); if
	 * (!Merchant_Id.equalsIgnoreCase("")) {
	 * query2.addCriteria(Criteria.where("merchantId").is(Merchant_Id)); } double j
	 * = mongoTemplate.count(query2, "tbl_transactionmaster"); double per = (j / i);
	 * double value = per * 100; js.put(Level, value); System.out.println(j); }
	 * return js.toMap(); }
	 */

	public Map<String, Object> getRiskLevelData(String Merchant_Id) {
		DistinctIterable<String> ds = mongoTemplate.getCollection("tbl_transactionmaster").distinct("riskLabel",
				String.class);
		Query query = new Query();
		JSONObject js = new JSONObject();
		query.addCriteria(Criteria.where("riskFlag").is("1"));
		System.out.print(Merchant_Id);
		if (!Merchant_Id.equalsIgnoreCase("")) {
			query.addCriteria(Criteria.where("merchantId").is(Merchant_Id));
		}
		double i = mongoTemplate.count(query, "tbl_transactionmaster");
		if (i <= 0) {
			js.put("error", "Merchant_Id Does not Exits");
			return js.toMap();
		}
		MongoCursor<String> results = ds.iterator();
		while (results.hasNext()) {
			System.out.println(i);
			Query query2 = new Query();
			String Level = results.next();
			query2.addCriteria(Criteria.where("riskFlag").is("1"));
			query2.addCriteria(Criteria.where("riskLabel").is(Level));
			if (!Merchant_Id.equalsIgnoreCase("")) {
				query2.addCriteria(Criteria.where("merchantId").is(Merchant_Id));
			}
			double j = mongoTemplate.count(query2, "tbl_transactionmaster");
			double per = (j / i);
			double value = per * 100;
			js.put(Level, value);
			System.out.println(j);
		}
		js.remove("");
		return js.toMap();
	}

	/*
	 * public Map<String, Object> getRiskCountData(String Merchant_Id,String Levels)
	 * { DistinctIterable<String> ds =
	 * mongoTemplate.getCollection("tbl_transactionmaster").distinct("rms_code",
	 * String.class); Query query = new Query(); JSONObject js = new JSONObject();
	 * 
	 * query.addCriteria(Criteria.where("rmsflag").is("1"));
	 * query.addCriteria(Criteria.where("rms_level").is(Levels));
	 * System.out.print(Merchant_Id); if (!Merchant_Id.equalsIgnoreCase("")) {
	 * query.addCriteria(Criteria.where("merchantId").is(Merchant_Id)); } double i =
	 * mongoTemplate.count(query, "tbl_transactionmaster");
	 * 
	 * if (i <= 0) { js.put("error", "Data Does not Exits"); return js.toMap(); }
	 * 
	 * MongoCursor<String> results = ds.iterator(); while (results.hasNext()) {
	 * System.out.println(i); Query query2 = new Query(); String Level =
	 * results.next(); query2.addCriteria(Criteria.where("rmsflag").is("1"));
	 * query2.addCriteria(Criteria.where("rms_code").is(Level));
	 * query2.addCriteria(Criteria.where("rms_level").is(Levels)); if
	 * (!Merchant_Id.equalsIgnoreCase("")) {
	 * query2.addCriteria(Criteria.where("merchantId").is(Merchant_Id)); } double j
	 * = mongoTemplate.count(query2, "tbl_transactionmaster"); js.put(Level, j);
	 * System.out.println(j); } return js.toMap(); }
	 */

	public Map<String, Object> getRiskCountData(String Merchant_Id, String Levels) {
		DistinctIterable<String> ds = mongoTemplate.getCollection("tbl_transactionmaster").distinct("riskCode",String.class);
		Query query = new Query();
		JSONObject js = new JSONObject();

		query.addCriteria(Criteria.where("riskFlag").is("1"));
		query.addCriteria(Criteria.where("riskLabel").is(Levels));
		System.out.print(Merchant_Id);
		if (!Merchant_Id.equalsIgnoreCase("")) {
			query.addCriteria(Criteria.where("merchantId").is(Merchant_Id));
		}
		double i = mongoTemplate.count(query, "tbl_transactionmaster");
		/*
		 * if (i <= 0) { js.put("error", "Data Does not Exits"); return js.toMap(); }
		 */
		MongoCursor<String> results = ds.iterator();
		while (results.hasNext()) {
			System.out.println(i);
			Query query2 = new Query();
			String Level = results.next();
			query2.addCriteria(Criteria.where("riskFlag").is("1"));
			query2.addCriteria(Criteria.where("riskCode").is(Level));
			query2.addCriteria(Criteria.where("riskLabel").is(Levels));
			if (!Merchant_Id.equalsIgnoreCase("")) {
				query2.addCriteria(Criteria.where("merchantId").is(Merchant_Id));
			}
			double j = mongoTemplate.count(query2, "tbl_transactionmaster");
			js.put(Level, j);
			System.out.println(j);
		}js.remove("");
		return js.toMap();
	}

	/*
	 * public List<TransactionMaster> GetRiskTransaction(String level, String Mid,
	 * String Riskcode) { final Query query = new Query(); final List<Criteria>
	 * criteria = new ArrayList<>(); if (!Mid.equals("")) {
	 * criteria.add(Criteria.where("merchantId").is(Mid)); } else if
	 * (!level.equalsIgnoreCase("")) {
	 * criteria.add(Criteria.where("rms_level").is(level)); } if
	 * (!Riskcode.equals("")) {
	 * criteria.add(Criteria.where("rms_code").is(Riskcode)); }
	 * System.out.print("Step 3:::::::::::::::: " + criteria); if
	 * (!criteria.isEmpty()) query.addCriteria(new
	 * Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
	 * System.out .print("Value 444 st step:::::::::::::::::::   " +
	 * mongoTemplate.find(query, TransactionMaster.class));
	 * 
	 * return mongoTemplate.find(query, TransactionMaster.class); }
	 */
	
	public List<TransactionMaster> GetRiskTransaction(String level, String Mid, String Riskcode) {
		final Query query = new Query();
		final List<Criteria> criteria = new ArrayList<>();
		if (!Mid.equals("")) {
			criteria.add(Criteria.where("merchantId").is(Mid));
		} if (!level.equalsIgnoreCase("")) {
			criteria.add(Criteria.where("riskLabel").is(level));
		}
		if (!Riskcode.equals("")) {
			criteria.add(Criteria.where("riskCode").is(Riskcode));
		}
		System.out.print("Step 3:::::::::::::::: " + criteria);
		if (!criteria.isEmpty())
			query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()]))).with(Sort.by(Direction.DESC,"dateTime"));
		System.out
				.print("Value 444 st step:::::::::::::::::::   " + mongoTemplate.find(query, TransactionMaster.class));

		return mongoTemplate.find(query, TransactionMaster.class);
	}

}
