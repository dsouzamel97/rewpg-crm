package com.crm.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class S2SCall {
	static Logger logger= LoggerFactory.getLogger(S2SCall.class);

	
	
	public static String ServerToServerCallINDUSS(String sURL, String data) {
		String line = null;
		BufferedReader br = null;
		StringBuffer respString = null;
		String ClentId="I3ZY102cb110dea1407e9dad6c54b037cf3bHXfZ";
		String ClientSecretKey="3inS4IXr9S1EqHD8raMAGKpDBWubMxxsddLY5H27";
		logger.info("S2SCall.java ::: secureServerCall() :: Posting URL : " + sURL);
		String auth = ClentId+ ":" + ClientSecretKey;
		byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
		String authHeaderValue = "Basic " + new String(encodedAuth);
		try
		{
			logger.info("INDUSSPAY::::::::::::::: "+authHeaderValue);
			URL obj = new URL(sURL);
			HttpsURLConnection con = (HttpsURLConnection)obj.openConnection();



			con.setRequestMethod("POST");
			con.addRequestProperty("Content-Type", "application/json");
			con.addRequestProperty("Authorization", authHeaderValue);

			con.setDoOutput(true);
			con.connect();

			OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());

			wr.write(data);
			wr.flush();

			respString = new StringBuffer();

			logger.info("S2SCall.java ::: secureServerCall() :: "+con.getResponseCode());
			logger.info("S2SCall.java ::: secureServerCall()res :: "+con.getResponseMessage());

			if (con.getResponseCode() == 200)
			{
				logger.info("S2SCall.java ::: secureServerCall() :: HTTP OK");
				br = new BufferedReader(
						new InputStreamReader(con.getInputStream()));

				while ((line = br.readLine()) != null)
				{
					logger.info("S2SCall.java ::: secureServerCall() :: Response : " + line);
					respString.append(line);
				}

				br.close();

				return respString.toString().trim();
			}
		}
		catch (Exception e)
		{
			logger.error("S2SCall.java ::: secureServerCall() :: Error Occurred while Processing Request : ", e);
		}


		return null;
	}
	
	public static String ServerToServerCallMerchant(String sURL, String data) {
		String line = null;
		BufferedReader br = null;
		StringBuffer respString = null;

		try
		{
			logger.info("INDUSSPAY::::::::::::::payput");
			URL obj = new URL(sURL);
			HttpsURLConnection con = (HttpsURLConnection)obj.openConnection();



			con.setRequestMethod("POST");
			con.addRequestProperty("Content-Type", "application/json");

			con.setDoOutput(true);
			con.connect();

			OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());

			wr.write(data);
			wr.flush();

			respString = new StringBuffer();

			logger.info("S2SCall.java ::: secureServerCall() :: "+con.getResponseCode());
			logger.info("S2SCall.java ::: secureServerCall()res :: "+con.getResponseMessage());

			if (con.getResponseCode() == 200)
			{
				logger.info("S2SCall.java ::: secureServerCall() :: HTTP OK");
				br = new BufferedReader(
						new InputStreamReader(con.getInputStream()));

				while ((line = br.readLine()) != null)
				{
					logger.info("S2SCall.java ::: secureServerCall() :: Response : " + line);
					respString.append(line);
				}

				br.close();

				return respString.toString().trim();
			}
		}
		catch (Exception e)
		{
			logger.error("S2SCall.java ::: secureServerCall() :: Error Occurred while Processing Request : ", e);
		}


		return null;
	}

}
